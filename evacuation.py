import math
import random
from random import randint
import numpy

evacuation_routes = []                                              # Evacuation Routes
set_of_humans = []                                                  # Set of Humans
capacity_route = []                                                 # Capacity of each Evacuation Route
evacuation_rate = []                                                # Evacuation Rate of each Evacuation Route
initial_evacuation_rate = []                                        # Needed for the MG update of the evacuation rate
agg_evacuation_rate = []                                            # Aggrandize the evacuation rates of all iterations
number_of_humans = []                                               # Number of humans already in Evacuation Route E(i)
distance_from_evac_routes = []                                      # Distance of human m from evacuation route e (2dimensional)
norm_rew_prob = []                                                  # Normalized Reward Probability
sla_iteration = 1

action_probability = []
mg_table = []                                                       # Minority games Table
mg_winning_table = []                                               # Minority Games Winning Actionv Table

mg_g = 0.8                                                         # Minority Game parameter learning rate - Small: Check the alternatives and not overflow
mg_evacuating_users = 0                                             # MG_Users who actually evacuate

# ******* Stochastic Learning Automata Algorithm description ************
#   1. SLA algorithm iterates in mixed strategy forms [ = Probabilistic Vectors ]
#   2. In the SLA algorithm, the game is played once in a slot according to the mixed strategy profile of the players
#   3. After each play, each player receives a payoff Rn(k) [kth slot] and updates its mixed strategy based on the received payoff --> Linear Reward Inaction
#   4. In particular if an action is selected and a postive payoff is received, the probability of choosing this action in the next slot increases.

#   Internal State of the Agent:
#   Probability distribution according to which actions would be chosen.
#   The probabilities of taking different actions would be adjusted according to
#   their previous successes and failures.

# ***********************************************************************


print("\n")

def create_evac_routes(number):
    for i in range(number) :
        evacuation_routes.append("route"+str(i+1))                        # [evac0, evac1, evac2, ...]
    return evacuation_routes
print ("The available evacuation routes are the following:")
print create_evac_routes(4)
print ("\n")



def create_setofhumans(number):
    for i in range(number) :
        set_of_humans.append("human"+str(i+1))                            # [h0, h1, h2, ...]
    return set_of_humans
print ("The humans ready for evacuation are the following:")
print create_setofhumans(137)
print ("\n")



def initialize_capacity() :                                                 # [10, 23, 54, ...] , capacity of each evacuation route
    length_cap_routes = len(evacuation_routes)
    for i in range(length_cap_routes) :
        capacity_route.append(randint(1,3))
    return capacity_route
print ("The corresponding capacity routes are the following:")
print initialize_capacity()
print ("\n")


def initialize_agg_evac_rate():
    length_cap_routes = len(evacuation_routes)
    for i in range(length_cap_routes):
        agg_evacuation_rate.append(0)                                       # Initialization of aggrandized evacuation rates,
    return agg_evacuation_rate                                              # Needed for the Reward Probability denominator




def initialize_evac_rate():                                                 # [0.123232, 0.54432, 0.45676, ...]
    length_cap_routes = len(evacuation_routes)
    for i in range(length_cap_routes) :
        evacuation_rate.append(random.random())                             # Produce a random float rate
        initial_evacuation_rate.append(0)
        initial_evacuation_rate[i] = evacuation_rate[i]
        agg_evacuation_rate[i] = (agg_evacuation_rate[i]+evacuation_rate[i])
    return evacuation_rate
initialize_agg_evac_rate()
print ("The evacuation rate is the following:")
print initialize_evac_rate()

print ("The aggrindezed evacuation rate is the following (Needed for the Reward Probability):")
print agg_evacuation_rate
print("\n")


def initialize_humans_in_evac() :
    length_cap_routes = len(evacuation_routes)
    for i in range(length_cap_routes):
        humans_in_route_i = 0                                                # Initialization with 0. No human in any evacuation route
        number_of_humans.append(humans_in_route_i)
    return number_of_humans
print ("Number of humans in each evacuation route initialization:")
print initialize_humans_in_evac()
print("\n")



def initialize_distance() :
    for j in range(len(set_of_humans)):
        distance_from_evac_routes.append([randint(1,20) for i in range(len(evacuation_routes))])     # [[3,4,6], [45,23,1], ...]
    return distance_from_evac_routes                                                                 # The first element monitors the distances of the first human from all evacuation routes

print("The distance of each human from each and every evacuation route :")
print initialize_distance()
print("\n")




def initialize_norm_pr_list() :
    for i in range(len(set_of_humans)):
        norm_rew_prob.append([0 for j in range(len(evacuation_routes))])
    return norm_rew_prob




# For all the evacuation routes, find all the probability rewards for each human
def normalized_reward_probability() :
    for i in range(len(set_of_humans)):
       for j in range(len(evacuation_routes)):
           rew_prob = (evacuation_rate[j]*(capacity_route[j]-number_of_humans[j]))/(distance_from_evac_routes[i][j] * (agg_evacuation_rate[j]/sla_iteration))
           norm_rew_prob[i][j] = rew_prob     # SOS: It has not yet been normalized!
    # print (norm_rew_prob)

    # Normalization process TODO: Ask about the denominator of the normalized probability

    for j in range(len(set_of_humans)):
        agg = 0  # needed local variable needed for the normalization process --> Denominator
        for i in range(len(evacuation_routes)):
            agg = agg + norm_rew_prob[j][i]
        for k in range(len(evacuation_routes)):
            norm_rew_prob[j][k] = norm_rew_prob[j][k]/agg
    print("The normalized Reward Probability of the each human is the following one:")
    print (norm_rew_prob)
    print("\n")
    return norm_rew_prob

initialize_norm_pr_list()
normalized_reward_probability()




# --- Action Probability ---
prob_step = 0.6                             # Step for the Action Probability Adjustment - Could be used as metric

def initialize_action_probability() :       # The initialization creates the same possibility for each and every evacuation route per human
    for i in range(len(set_of_humans)):     # Form : [ human1[route1,route2,...] , human2[route1,route2,...] , ....]
        action_probability.append([1.0/len(evacuation_routes) for j in range(len(evacuation_routes))])
    return action_probability

initialize_action_probability()
print("Initialization of Action Probability - The same for all humans in the 1st SLA Iteration:")
print(action_probability)
print("\n")



# TODO: Here we will have the chosen action of each player and the reward probability. For each human we will update the mixed strategy
# After the action probability vector is updated, the user will choose the evacuation route for the next iteration
# def update_action_probability(TODO:We will have parameters) :   --> This function will operate for EACH human...



route_list = []
def initialize_list_route():
    for i in range(len(evacuation_routes)):
        route_list.append(i+1)
    return route_list

initialize_list_route()
humans_want_route = [0] * len(evacuation_routes)                              # Represents how many humans want to go to each evacuation route respectively
mg_winning_table = [0] * len(evacuation_routes)
def initiate_humans_want_route():
    for i in range(len(evacuation_routes)):
        humans_want_route[i] = 0
        mg_winning_table[i] = 0
    return humans_want_route

selected_routes = [0] * len(set_of_humans)                                    # Represents in which route, each human wants to go; i.e [1,3,2,...] --> The first human in the 1st route, ....
def choose_evacuation_route():                                                # Each human chooses the route with the greatest mixed strategy
    initiate_humans_want_route()
    for i in range(len(set_of_humans)):
        selected_routes[i] = numpy.random.choice(route_list,p=action_probability[i])
        humans_want_route[selected_routes[i]-1] = humans_want_route[selected_routes[i]-1] + 1
    return selected_routes

choose_evacuation_route()
print("The selected route for each human:")
print selected_routes
print("\n")
print("Number of humans that want to go to the Evacuation Route i:")
print humans_want_route
print("\n")

# *** Up to here : Humans have selected where they WANT to go. It does not mean that they will actually go. ***




# ************* Minority Games - Distributed Decision Making ************
# TODO: Remember! If I need which evacuation route chose a human I have already created such a list! --> selected_routes[]
# MG : Decides  who will go where

# Possible actions a(m) = 0 --> do not evacuate
#                  a(m) = 1 --> evacuate

mg_iteration = 1        # iteration of minority games
chosen_action = []


def chosen_actions() :
    for i in range(len(set_of_humans)):
        chosen_action.append(-1)
    return chosen_action
chosen_actions()


def initialize_mgtable() :
    for i in range(len(set_of_humans)):
        mg_table.append([0 for j in range(6)])         # Dimensions human*6
        mg_table[i][0] = 0              # Do NOT evacuate!
        mg_table[i][1] = 0.5            # Initialize with 0.5
        mg_table[i][2] = 0
        mg_table[i][3] = 1              # Evacuate!
        mg_table[i][4] = 0.5            # Initialize with 0.5
        mg_table[i][5] = 0
    return mg_table
print (initialize_mgtable())


def mg_choose_action():
    for i in range(len(set_of_humans)):
        temp_list = [mg_table[i][0],mg_table[i][3]]
        minimum_index = temp_list.index(min(temp_list))
        minimum_prob = min(mg_table[1],mg_table[4])
        random_number = random.uniform(0.000,1.0000)
        #print ("Random:"+str(random_number))
        chosen_action[i] = numpy.random.choice(temp_list, p=[mg_table[i][1], mg_table[i][4]])  # Choose by weighting the probabilities
        #print ("Chosen Action:"+str(chosen_action[i]))
    return chosen_action

# The table that defines the chosen action of each human has now been created:
# [ h1(0) , h2(0) , h3(1) , ... ]
# The first human wants not to evacuate, the second the same, the third wants to evacuate, ...

# The next step is to find the winning action and to update the users' probabilities



def find_winning_action_update() :
    convergence = 0                             # boolean variable to check if we have a convergence
    mg_iteration = 1
    while (convergence!=1):                     # Minority Game convergence condition
        mg_choose_action()
        for i in range(len(humans_want_route)):
            temp = humans_want_route[i]
            not_evacuate = 0
            evacuate = 0
            k = 0
            while(temp !=0 ):                       # Finding the winning action process
                if (selected_routes[k] == i+1):
                    temp -= 1
                    if(chosen_action[k] == 0):
                        not_evacuate += 1
                    else:
                        evacuate += 1
                    k += 1  # We have the kth human that wants to go to that route

                else:
                    k += 1
            if(evacuate>capacity_route[i]):          # Define which is the winning action, according to the capacity threshold
                winning_action = 0
            else:
                winning_action = 1
            #print("Winning:"+str(winning_action))

            mg_winning_table[i] = winning_action        # Update the winning action for each route

            for l in range(len(chosen_action)):      # Update the Probabilities
                if(selected_routes[l] == i+1):
                    if(chosen_action[l] == winning_action):     # Each human updates its accumulated score
                        # Update positive
                        if (winning_action == 0):
                            mg_table[l][2] += 1
                        elif (winning_action == 1):
                            mg_table[l][5] += 1



                    print("ite "+str(mg_iteration))
                    denominator = math.exp(mg_g*mg_table[l][2]) +math.exp(mg_g* mg_table[l][5])
                    mg_table[l][1] = math.exp(mg_g*mg_table[l][2])/denominator
                    mg_table[l][4] = math.exp(mg_g*mg_table[l][5])/denominator

                    # ******     Checking for Overflow - Adapting the values to avoid it and converge ***********
                    if mg_table[l][1] < 0.01:
                        mg_table[l][1] = 0.01
                    if mg_table[l][4] < 0.01:
                        mg_table[l][4] = 0.01
                    if mg_table[l][1] > 0.99:
                        mg_table[l][1] = 0.99
                    if mg_table[l][4] > 0.99:
                        mg_table[l][4] = 0.99


    # ---- Check if we have Minority Game convergence !!! ----
        for p in range(len(selected_routes)):
            if(abs(mg_table[p][1]-1)<0.2 or abs(mg_table[p][4]-1)<0.2 ):
                if (p == len(selected_routes)-1):
                    convergence = 1
                    print("Minority Game is converged...")
            else:
                mg_iteration += 1
                break
    print ("Minority Game overall iterations: "+ str(mg_iteration))
    return convergence



find_winning_action_update()                        # Run Minority Game Algorithm
print("Minority Game final probability array:")
print mg_table

print("Minority Game final winning Action:")
print mg_winning_table

print("Minority Game humans' final choices:")
print chosen_action


def mg_update_actions() :
    winning_selected = []
    for i in range(len(humans_want_route)):
        mg_evacuating_users = 0                     # Initialize the users that want to evacuate in every different evacuation route
        acc_temp = humans_want_route[i]             # Check how many humans want each evacuation route

        for j in range(len(selected_routes)):
            if(selected_routes[j]==i+1 and acc_temp!=0):            # Check if the j-th human wants the i-th evacuation route
                acc_temp = acc_temp - 1
                if (chosen_action[j] == mg_winning_table[i]) :           # If the j-th human finally chose the winning action
                    if(mg_winning_table[i] == 1):                        # The winning action is to evacuate!
                        # Here we should put out this human from all the tables and to update the mg humans that are evacuating.
                        mg_evacuating_users = mg_evacuating_users + 1
                        winning_selected.append(j)


        # ---------------------------------------- Minority Games Updates -----------------------------------------
        # We have to update the number of humans in each evacuation route after the minority game
        number_of_humans[i] = number_of_humans[i] - int(evacuation_rate[i]*number_of_humans[i]) + mg_evacuating_users  # int(float x) --> Round down to nearest integer

        # We have to update the evacuation rate of each evacuation route
        evacuation_rate[i] = initial_evacuation_rate[i]*((capacity_route[i]-number_of_humans[i])/capacity_route[i])
        # ----------------------------------------------------------------------------------------------------------

    # ------ Process in order to extract from all the lists the humans that decided to evacuate ------
    for k in range(len(winning_selected)):
        selected_routes[winning_selected[k]] = -1
        chosen_action[winning_selected[k]] = -1
        set_of_humans[winning_selected[k]] = -1
        distance_from_evac_routes[winning_selected[k]] = -1
        action_probability[winning_selected[k]] = -1
        mg_table[winning_selected[k]] = -1
        norm_rew_prob[winning_selected[k]] = -1


    # Slicing in order to remove without the need to build a new python object
    selected_routes[:] = (value for value in selected_routes if value != -1)
    chosen_action[:] = (value for value in chosen_action if value != -1)
    set_of_humans[:] = (value for value in set_of_humans if value != -1)
    distance_from_evac_routes[:] = (value for value in distance_from_evac_routes if value != -1)
    action_probability[:] = (value for value in action_probability if value != -1)
    mg_table[:] = (value for value in mg_table if value != -1)
    norm_rew_prob[:] = (value for value in norm_rew_prob if value != -1)


print("*********************************************************")
mg_update_actions()




'''
#------------------------------------- RUN THE SLA ----------------------------------------------------------
sla_convergence = 0                             # Boolean variable to check if we have an SLA convergence
time_slot = 10                                  # mg_ite<sla_ite<t  TODO: Ask about the overall time slot t
while (sla_convergence != 1):

    choose_evacuation_route()                           # According to the Action Probabilities choose the desired route
    print("The selected route for each human:")
    print selected_routes
    print("\n")

    print("Number of humans that want to go to the Evacuation Route i:")
    print humans_want_route
    print("\n")

    # Running the Minority Game module in order to find if they will finally evacuate or not . . .
    print("Running the Minority Game . . . ")
    find_winning_action_update()
    print("\n")

    for i in range(len(chosen_action)):
        temp = i+1
        print("Human["+str(temp)+"] wants : "+str(chosen_action[i]))

    # Update the number of humans that are in each evacuation route
    for i in range(len(evacuation_routes)):
        number_of_humans[i] = number_of_humans[i] - evacuation_rate[i]*time_slot + humans_want_route[i]

#print find_winning_action_update()


#for i in range(len(chosen_action)):
#    temp = i+1
#    print ("Human["+str(temp)+"] wants : "+str(chosen_action[i]))

'''
# ------------------------------------------------------------------------------------------------------------------------------------

