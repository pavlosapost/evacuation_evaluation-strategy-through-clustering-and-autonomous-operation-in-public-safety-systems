import math
import random
from random import randint
import numpy
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties

evacuation_routes = []                                              # Evacuation Routes
set_of_humans = []                                                  # Set of Humans
capacity_route = []                                                 # Capacity of each Evacuation Route
evacuation_rate = []                                                # Evacuation Rate of each Evacuation Route
initial_evacuation_rate = []                                        # Needed for the MG update of the evacuation rate
agg_evacuation_rate = []                                            # Aggrandize the evacuation rates of all iterations
number_of_humans = []                                               # Number of humans already in Evacuation Route E(i)
distance_from_evac_routes = []                                      # Distance of human m from evacuation route e (2dimensional)
initial_dist = []
norm_rew_prob = []                                                  # Normalized Reward Probability
agg_rew = []

action_probability = []
mg_table = []                                                       # Minority games Table
mg_minority_table = []                                               # Minority Games Winning Actionv Table

mg_g = 0.8
prob_step = 0.7
# Minority Game parameter learning rate - Small: Check the alternatives and not overflow
#mg_evacuating_users = 0                                             # MG_Users who actually evacuate

# ******* Stochastic Learning Automata Algorithm description ************
#   1. SLA algorithm iterates in mixed strategy forms [ = Probabilistic Vectors ]
#   2. In the SLA algorithm, the game is played once in a slot according to the mixed strategy profile of the players
#   3. After each play, each player receives a payoff Rn(k) [kth slot] and updates its mixed strategy based on the received payoff --> Linear Reward Inaction
#   4. In particular if an action is selected and a postive payoff is received, the probability of choosing this action in the next slot increases.

#   Internal State of the Agent:
#   Probability distribution according to which actions would be chosen.
#   The probabilities of taking different actions would be adjusted according to
#   their previous successes and failures.

# ***********************************************************************


print("\n")

def create_evac_routes(number):
    for i in range(number) :
        evacuation_routes.append(i+1)                        # [evac0, evac1, evac2, ...]
    return evacuation_routes
print ("The available evacuation routes are the following:")
print create_evac_routes(4)
print ("\n")



def create_setofhumans(number):
    for i in range(number) :
        set_of_humans.append("human"+str(i+1))                            # [h0, h1, h2, ...]
    return set_of_humans
print ("The humans ready for evacuation are the following:")
print create_setofhumans(331)
print ("\n")



def initialize_capacity() :                                                 # [10, 23, 54, ...] , capacity of each evacuation route
    length_cap_routes = len(evacuation_routes)
    for i in range(length_cap_routes) :
        capacity_route.append(randint(9,14))
    return capacity_route
print ("The corresponding capacity routes are the following:")
print initialize_capacity()
print ("\n")


def initialize_agg_evac_rate():
    length_cap_routes = len(evacuation_routes)
    for i in range(length_cap_routes):
        agg_evacuation_rate.append(0)                                       # Initialization of aggrandized evacuation rates,
    return agg_evacuation_rate                                              # Needed for the Reward Probability denominator





def initialize_evac_rate():                                                 # [0.123232, 0.54432, 0.45676, ...]
    length_cap_routes = len(evacuation_routes)
    for i in range(length_cap_routes) :
        evacuation_rate.append(random.uniform(0.2, 0.7))                             # Produce a random float rate
        initial_evacuation_rate.append(0)
        initial_evacuation_rate[i] = evacuation_rate[i]
        agg_evacuation_rate[i] = (agg_evacuation_rate[i]+evacuation_rate[i])
    return evacuation_rate
initialize_agg_evac_rate()
print ("The evacuation rate is the following:")
print initialize_evac_rate()

print ("The aggrindezed evacuation rate is the following (Needed for the Reward Probability):")
print agg_evacuation_rate
print("\n")


def initialize_humans_in_evac() :
    length_cap_routes = len(evacuation_routes)
    for i in range(length_cap_routes):
        humans_in_route_i = 0                                                # Initialization with 0. No human in any evacuation route
        number_of_humans.append(humans_in_route_i)
    return number_of_humans
print ("Number of humans in each evacuation route initialization:")
print initialize_humans_in_evac()
print("\n")



def initialize_distance() :
    for j in range(len(set_of_humans)):
        distance_from_evac_routes.append([randint(10,15) for i in range(len(evacuation_routes))])     # [[3,4,6], [45,23,1], ...]
        initial_dist.append([0 for k in range(len(evacuation_routes))])
    for i in range(len(set_of_humans)):
        for j in range(len(evacuation_routes)):
            initial_dist[i][j] = distance_from_evac_routes[i][j]
    return distance_from_evac_routes                                                                 # The first element monitors the distances of the first human from all evacuation routes

print("The distance of each human from each and every evacuation route :")
print initialize_distance()
print("\n")




def initialize_norm_pr_list() :
    for i in range(len(set_of_humans)):
        norm_rew_prob.append([0.0 for j in range(len(evacuation_routes))])
        agg_rew.append([0.0 for j in range(len(evacuation_routes))])

    print("Initialization")
    print norm_rew_prob
    return norm_rew_prob

initialize_norm_pr_list()


# For all the evacuation routes, find all the probability rewards for each human
def reward_probability(sla_iteration) :
    for i in range(len(set_of_humans)):
        for j in range(len(evacuation_routes)):
            print("Humans already:"+str(number_of_humans[j]))
            if(number_of_humans[j]!=0):
                norm_rew_prob[i][j] = ((evacuation_rate[j]*((1.0*capacity_route[j])/number_of_humans[j]))*1.0)/(distance_from_evac_routes[i][j] *((1.0*agg_evacuation_rate[j]/sla_iteration)))
            else:
                norm_rew_prob[i][j] = ((evacuation_rate[j]*((1.0*capacity_route[j])-number_of_humans[j]))*1.0)/(distance_from_evac_routes[i][j] *((1.0*agg_evacuation_rate[j]/sla_iteration)))

    return norm_rew_prob
    # Normalization process TODO: Ask about the denominator of the normalized probability



def normalized_reward_probability() :
    for j in range(len(set_of_humans)):
        agg = 0.0 # needed local variable needed for the normalization process --> Denominator
        for i in range(len(evacuation_routes)):
            agg = agg + norm_rew_prob[j][i]
        for k in range(len(evacuation_routes)):
            norm_rew_prob[j][k] = (1.0*norm_rew_prob[j][k])/agg
    print("The normalized Reward Probability of the each human is the following one:")
    print (norm_rew_prob)
    print("\n")
    return norm_rew_prob




def initialize_action_probability() :       # The initialization creates the same possibility for each and every evacuation route per human
    for i in range(len(set_of_humans)):     # Form : [ human1[route1,route2,...] , human2[route1,route2,...] , ....]
        action_probability.append([1.0/len(evacuation_routes) for j in range(len(evacuation_routes))])
    return action_probability

initialize_action_probability()
print("Initialization of Action Probability - The same for all humans in the 1st SLA Iteration:")
print(action_probability)
print("\n")


def update_action_probability(human, chosen_route,sla_iteration) :
    for j in range(len(evacuation_routes)):
        if(evacuation_routes[j] == chosen_route):
            action_probability[human][j] = action_probability[human][j] + prob_step*norm_rew_prob[human][j]*(1-action_probability[human][j])
        else:
            action_probability[human][j] = action_probability[human][j] - prob_step*norm_rew_prob[human][j]*action_probability[human][j]
    action_probability[human] = (action_probability[human]/(numpy.sum(action_probability[human]))).tolist()





# TODO: Here we will have the chosen action of each player and the reward probability. For each human we will update the mixed strategy
# After the action probability vector is updated, the user will choose the evacuation route for the next iteration
# def update_action_probability(TODO:We will have parameters) :   --> This function will operate for EACH human...



route_list = []
def initialize_list_route():
    for i in range(len(evacuation_routes)):
        route_list.append(i+1)
    return route_list

initialize_list_route()
humans_want_route = [0] * len(evacuation_routes)                              # Represents how many humans want to go to each evacuation route respectively
mg_minority_table = [0] * len(evacuation_routes)
def initiate_humans_want_route():
    for i in range(len(evacuation_routes)):
        humans_want_route[i] = 0
        mg_minority_table[i] = 0
    return humans_want_route

selected_routes = [0] * len(set_of_humans)                                    # Represents in which route, each human wants to go; i.e [1,3,2,...] --> The first human in the 1st route, ....
def choose_evacuation_route():                                                # Each human chooses the route with the greatest mixed strategy
    initiate_humans_want_route()
    for i in range(len(set_of_humans)):
        selected_routes[i] = numpy.random.choice(route_list,p=action_probability[i])
        humans_want_route[selected_routes[i]-1] = humans_want_route[selected_routes[i]-1] + 1
    return selected_routes




# ************* Minority Games - Distributed Decision Making ************
# TODO: Remember! If I need which evacuation route chose a human I have already created such a list! --> selected_routes[]
# MG : Decides  who will go where

# Possible actions a(m) = 0 --> do not evacuate
#                  a(m) = 1 --> evacuate

chosen_action = []


def chosen_actions() :
    for k in range(len(set_of_humans)):
        chosen_action.append(-2)
    return chosen_action
chosen_actions()


def initialize_mgtable() :
    for i in range(len(set_of_humans)):
        mg_table.append([0 for j in range(6)])         # Dimensions human*6
        mg_table[i][0] = 0              # Do NOT evacuate!
        mg_table[i][1] = 0.5            # Initialize with 0.5
        mg_table[i][2] = 0
        mg_table[i][3] = 1              # Evacuate!
        mg_table[i][4] = 0.5            # Initialize with 0.5
        mg_table[i][5] = 0
    return mg_table
initialize_mgtable()

def reform_mgtable():
    for i in range(len(set_of_humans)):
        mg_table[i][0] = 0  # Do NOT evacuate!
        mg_table[i][1] = 0.5  # Initialize with 0.5
        mg_table[i][2] = 0
        mg_table[i][3] = 1  # Evacuate!
        mg_table[i][4] = 0.5  # Initialize with 0.5
        mg_table[i][5] = 0
    return mg_table


def mg_choose_action(route):
    for k in range(len(selected_routes)):
        if(selected_routes[k] == route+1):
            temp_list = [mg_table[k][0],mg_table[k][3]]
            chosen_action[k] = numpy.random.choice(temp_list, p=[mg_table[k][1], mg_table[k][4]])  # Choose by weighting the probabilities

    #print chosen_action
    return chosen_action

# The table that defines the chosen action of each human has now been created:
# [ h1(0) , h2(0) , h3(1) , ... ]
# The first human wants not to evacuate, the second the same, the third wants to evacuate, ...

# The next step is to find the winning action and to update the users' probabilities

def find_winning_action_update_versiontwo(sla_iteration):
    for c in range(len(humans_want_route)):
        print("ITERATION:"+str(c))
        convergence = 0
        players = []
        mg_iteration = 1
        evacuating_users_temp = 0                       # How many will actually evacuate in Route i
        if(number_of_humans[c]<capacity_route[c]):      # Only if this condition applies for the Route, run the Minority Game
            while(convergence != 1):
                mg_choose_action(c)
                not_evacuate = 0
                evacuate = 0
                for k in range(len(selected_routes)):                       # Finding the winning action process
                    if (selected_routes[k] == c+1):
                        players.append(k)
                        if(chosen_action[k] == 0):                          # Chosen Action : What humank wants to do?
                            not_evacuate += 1
                        else:
                            evacuate += 1

                if (evacuate + number_of_humans[c] > capacity_route[c]):  # Define which is the winning action, according to the capacity threshold
                    minority_action = 0
                    mg_minority_table[c] = minority_action  # Update the winning action for each route
                else:
                    minority_action = 1
                    mg_minority_table[c] = minority_action  # Update the winning action for each route

                for l in range(len(chosen_action)):      # Update the Probabilities
                    if(selected_routes[l] == c+1):
                        if(chosen_action[l] == minority_action):     # Each human updates its accumulated score
                            if (minority_action == 0):
                                mg_table[l][2] += 1
                            elif (minority_action == 1):
                                mg_table[l][5] += 1

                for p in range(len(selected_routes)):
                    if (selected_routes[p] == c + 1):
                        denominator = math.exp(mg_g * mg_table[p][2]) + math.exp(mg_g * mg_table[p][5])
                        mg_table[p][1] = math.exp(mg_g * mg_table[p][2]) / (1.0 * denominator)
                        mg_table[p][4] = math.exp(mg_g * mg_table[p][5]) / (1.0 * denominator)

                mg_count = 0
                for p in range(len(selected_routes)):
                    if (selected_routes[p] == c + 1):

                        if mg_table[p][1] < 0.01:
                            mg_table[p][1] = 0.01
                        if mg_table[p][4] < 0.01:
                            mg_table[p][4] = 0.01
                        if mg_table[p][1] > 0.99:
                            mg_table[p][1] = 0.99
                        if mg_table[p][4] > 0.99:
                            mg_table[p][4] = 0.99

                for p in range(len(players)):
                    if (abs(mg_table[players[p]][1] - 1) < 0.2 or abs(mg_table[players[p]][4] - 1) < 0.2):
                        mg_count += 1

                if (mg_count == len(players)):
                    convergence = 1
                    #conv_counter -= 1
                    print ("MG TABLE:"+str(mg_table))
                    print("Evacuating & thres_Convergence:"+str(evacuating_users_temp)+str(":::")+str(capacity_route[c]-number_of_humans[c]))
                    print("Minority Game is converged...")
                else:
                    mg_iteration += 1
                    print("Evacuating & thres_NOTConvergence:"+str(evacuating_users_temp)+str(":::")+str(capacity_route[c]-number_of_humans[c]))

            evacuating_selected = []
            evacuating_users = 0  # Initialize the users that want to evacuate in every different evacuation route
            for q in range(len(selected_routes)):
                if (selected_routes[q] == c + 1):     # Check if the j-th human wants the i-th evacuation route
                    if (chosen_action[q] == 1):       # If the j-th human finally chose the winning action
                        evacuating_users = evacuating_users + 1
                        # print("Evacuating users:"+str(evacuating_users))
                        evacuating_selected.append(q)
                        if (number_of_humans[c] != 0):
                            reward_pr1.append(((evacuation_rate[c] * ((1.0 * capacity_route[c]) / number_of_humans[c])) * 1.0) / (distance_from_evac_routes[q][c] * ((1.0 * agg_evacuation_rate[c] / sla_iteration))))
                        else:
                            reward_pr1.append(((evacuation_rate[c] * ((1.0 * capacity_route[c]) - number_of_humans[c])) * 1.0) / (distance_from_evac_routes[q][c] * ((1.0 * agg_evacuation_rate[c] / sla_iteration))))

            evacuating_users_temp = evacuating_users


            for k in range(len(evacuating_selected)):
                selected_routes[evacuating_selected[k]] = -3
                chosen_action[evacuating_selected[k]] = -3
                set_of_humans[evacuating_selected[k]] = -3
                distance_from_evac_routes[evacuating_selected[k]] = -3
                action_probability[evacuating_selected[k]] = -3
                mg_table[evacuating_selected[k]] = -3
                norm_rew_prob[evacuating_selected[k]] = -3

            selected_routes[:] = (value for value in selected_routes if value != -3)
            chosen_action[:] = (value for value in chosen_action if value != -3)
            set_of_humans[:] = (value for value in set_of_humans if value != -3)
            distance_from_evac_routes[:] = (value for value in distance_from_evac_routes if value != -3)
            action_probability[:] = (value for value in action_probability if value != -3)
            mg_table[:] = (value for value in mg_table if value != -3)
            norm_rew_prob[:] = (value for value in norm_rew_prob if value != -3)

        if(number_of_humans[c]<capacity_route[c]):
            number_of_humans[c] = number_of_humans[c] - int(math.ceil(evacuation_rate[c] * number_of_humans[c])) + evacuating_users_temp

        elif(number_of_humans[c]>=capacity_route[c]):
            number_of_humans[c] -= int(math.ceil(evacuation_rate[c] * number_of_humans[c]))

        # We have to update the evacuation rate of each evacuation route

        if(number_of_humans[c]<capacity_route[c]):
            evacuation_rate[c] = initial_evacuation_rate[c] * ((capacity_route[c] - number_of_humans[c]) / (1.0 * (capacity_route[c])))

        elif(number_of_humans[c]>capacity_route[c]):
            evacuation_rate[c] = initial_evacuation_rate[c] *(1.0*capacity_route[c]/number_of_humans[c])

        agg_evacuation_rate[c] = agg_evacuation_rate[c] + evacuation_rate[c]
        #number_of_humans[i] = number_of_humans[i] - int(evacuation_rate[i] * number_of_humans[i]) + evacuating_users_temp



    reward_probability(sla_iteration)
    normalized_reward_probability()

    for o in range(len(set_of_humans)):
        chosen_route = selected_routes[o]
        update_action_probability(o,chosen_route,sla_iteration)




print("*********************************************************")










def sla():
    sla_convergence = 0  # Boolean variable to check if we have an SLA convergence\
    sla_iteration = 1
    iteration_list1.append(1)
    while (sla_convergence != 1):

        choose_evacuation_route()
        temp_r1 = 0
        temp_r2 = 0
        temp_r3 = 0
        temp_r4 = 0
        temp_er1 = 0
        temp_er2 = 0
        temp_er3 = 0
        temp_er4 = 0
        print("SLA NUMBER OF HUMANS:"+str(number_of_humans))
        for k in range(len(humans_want_route)):
            if (k == 0):
                temp_r1 = number_of_humans[k]
            elif (k == 1):
                temp_r2 = number_of_humans[k]
            elif (k == 2):
                temp_r3 = number_of_humans[k]
            else:
                temp_r4 = number_of_humans[k]

        for k in range(len(humans_want_route)):
            if (k == 0):
                temp_er1 = evacuation_rate[k]
            elif (k == 1):
                temp_er2 = evacuation_rate[k]
            elif (k == 2):
                temp_er3 = evacuation_rate[k]
            else:
                temp_er4 = evacuation_rate[k]

        cm1_route1.append(temp_r1)
        cm1_route2.append(temp_r2)
        cm1_route3.append(temp_r3)
        cm1_route4.append(temp_r4)
        cm1_er1.append(temp_er1)
        cm1_er2.append(temp_er2)
        cm1_er3.append(temp_er3)
        cm1_er4.append(temp_er4)


        print("The selected routes in SLA are:"+str(selected_routes))

        # Run the Minority Game algorithm...
        find_winning_action_update_versiontwo(sla_iteration)
        print("Minority Game final probability array:")
        print mg_table
        print("\n")


        print("Updating the SLA corresponding probabilities...")
        #mg_update_actions()
        reform_mgtable()
        #Check if we have SLA convergence . . .
        sla_count_human = 0
        for w in range(len(set_of_humans)):
            break_temp = 1
            for x in range(len(evacuation_routes)):


                if (action_probability[w][x] >= 0.9):
                    if(break_temp==1):
                        break_temp -= 1
                        sla_count_human = sla_count_human + 1
                    #break
        if (sla_count_human == len(set_of_humans)):
            if(len(action_probability)!=0):
                for u in range(len(action_probability)):
                    max_index = action_probability[u].index(max(action_probability[u]))
                    if (number_of_humans[max_index] != 0):
                        reward_pr1.append(((evacuation_rate[max_index] * ((1.0 * capacity_route[max_index]) / number_of_humans[max_index])) * 1.0) / (distance_from_evac_routes[u][max_index] * ((1.0 * agg_evacuation_rate[max_index] / sla_iteration))))
                    else:
                        reward_pr1.append(((evacuation_rate[max_index] * ((1.0 * capacity_route[max_index]) - number_of_humans[max_index])) * 1.0) / ( distance_from_evac_routes[u][max_index] * ( (1.0 * agg_evacuation_rate[max_index] / sla_iteration))))

            #print (sla_count_human == len(set_of_humans))
            sla_convergence = 1
            # Find the average Normalized Reward

            print ("SLA CONVERGENCE . . . ")

        else:

            sla_iteration += 1
            iteration_list1.append(sla_iteration)
        print action_probability
        print(len(action_probability))
        print sla_iteration

# -----------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------
# **************************** SLA with 50% probability to evacuate or not evacuate ********************************
def mg_choose_action_random(route):
    for k in range(len(selected_routes)):
        if(selected_routes[k] == route+1):
            temp_list = [0,1]
            chosen_action[k] = numpy.random.choice(temp_list, p=[0.5, 0.5])

    #print chosen_action
    return chosen_action

def find_winning_action_update_random(sla_iteration):
    for i in range(len(humans_want_route)):
        players = []
        evacuating_users_temp = 0                       # How many will actually evacuate in Route i
        #print("SLA ITERATION:"+str(sla_iteration))
        print("Number_of_humans:"+str(number_of_humans))
        print("Capacity:"+str(capacity_route))
        if(number_of_humans[i]<capacity_route[i]):      # Only if this condition applies for the Route, run the Minority Game
            mg_choose_action_random(i)
            not_evacuate = 0
            evacuate = 0
            for k in range(len(selected_routes)):                       # Finding the winning action process
                if (selected_routes[k] == i+1):
                    players.append(k)
                    if(chosen_action[k] == 0):                          # Chosen Action : What humank wants to do?
                        not_evacuate += 1
                    else:
                        evacuate += 1

            print("EVACUAAAAATE IN ITERATION:"+str(sla_iteration)+str("   :")+str(evacuate))
            evacuating_selected = []
            evacuating_users = 0  # Initialize the users that want to evacuate in every different evacuation route
            for q in range(len(selected_routes)):
                if (selected_routes[q] == i + 1):     # Check if the j-th human wants the i-th evacuation route
                    if (chosen_action[q] == 1):       # If the j-th human finally chose the winning action
                        evacuating_users = evacuating_users + 1
                        # print("Evacuating users:"+str(evacuating_users))
                        evacuating_selected.append(q)
                        if (number_of_humans[i] != 0):
                            reward_pr2.append(((evacuation_rate[i] * ((1.0 * capacity_route[i]) / number_of_humans[i])) * 1.0) / (distance_from_evac_routes[q][i] * ((1.0 * agg_evacuation_rate[i] / sla_iteration))))
                        else:
                            reward_pr2.append(((evacuation_rate[i] * ((1.0 * capacity_route[i]) - number_of_humans[i])) * 1.0) / (distance_from_evac_routes[q][i] * ((1.0 * agg_evacuation_rate[i] / sla_iteration))))
            evacuating_users_temp = evacuating_users

            for k in range(len(evacuating_selected)):
                selected_routes[evacuating_selected[k]] = -3
                chosen_action[evacuating_selected[k]] = -3
                set_of_humans[evacuating_selected[k]] = -3
                distance_from_evac_routes[evacuating_selected[k]] = -3
                action_probability[evacuating_selected[k]] = -3
                norm_rew_prob[evacuating_selected[k]] = -3

            selected_routes[:] = (value for value in selected_routes if value != -3)
            chosen_action[:] = (value for value in chosen_action if value != -3)
            set_of_humans[:] = (value for value in set_of_humans if value != -3)
            distance_from_evac_routes[:] = (value for value in distance_from_evac_routes if value != -3)
            action_probability[:] = (value for value in action_probability if value != -3)
            norm_rew_prob[:] = (value for value in norm_rew_prob if value != -3)

        number_of_humans[i] = number_of_humans[i] - int(evacuation_rate[i] * number_of_humans[i]) + evacuating_users_temp

        # We have to update the evacuation rate of each evacuation route

        if(number_of_humans[i]<capacity_route[i]):
            evacuation_rate[i] = initial_evacuation_rate[i] * ((capacity_route[i] - number_of_humans[i]) / (1.0 * (capacity_route[i])))

        elif(number_of_humans[i]>capacity_route[i]):
            evacuation_rate[i] = initial_evacuation_rate[i] *(1.0*capacity_route[i]/number_of_humans[i])

        agg_evacuation_rate[i] = agg_evacuation_rate[i] + evacuation_rate[i]
        #number_of_humans[i] = number_of_humans[i] - int(evacuation_rate[i] * number_of_humans[i]) + evacuating_users_temp


    reward_probability(sla_iteration)
    normalized_reward_probability()

    for o in range(len(set_of_humans)):
        chosen_route = selected_routes[o]
        update_action_probability(o,chosen_route,sla_iteration)

def sla_random_choose():
    sla_convergence = 0  # Boolean variable to check if we have an SLA convergence\
    sla_iteration = 1
    iteration_list2.append(1)
    while (sla_convergence != 1):
        print("RAAAAAAANDOOOOOOOOOM")
        choose_evacuation_route()
        print("The selected routes in SLA are:"+str(selected_routes))


        # Find the cluster size for each route for each iteration
        temp_r11 = 0
        temp_r21 = 0
        temp_r31 = 0
        temp_r41 = 0
        temp_er1 = 0
        temp_er2 = 0
        temp_er3 = 0
        temp_er4 = 0
        '''
        for k in range(len(selected_routes)):
            if (selected_routes[k] == 1):
                temp_r11 += 1
            elif (selected_routes[k] == 2):
                temp_r21 += 1
            elif (selected_routes[k] == 3):
                temp_r31 += 1
            else:
                temp_r41 += 1
        '''
        for k in range(len(humans_want_route)):
            if (k == 0):
                temp_r11 = number_of_humans[k]
            elif (k == 1):
                temp_r21 = number_of_humans[k]
            elif (k == 2):
                temp_r31 = number_of_humans[k]
            else:
                temp_r41 = number_of_humans[k]

        for k in range(len(humans_want_route)):
            if (k == 0):
                temp_er1 = evacuation_rate[k]
            elif (k == 1):
                temp_er2 = evacuation_rate[k]
            elif (k == 2):
                temp_er3 = evacuation_rate[k]
            else:
                temp_er4 = evacuation_rate[k]
        print("This is the sum:"+str(temp_r11+temp_r21+temp_r31+temp_r41))
        cm2_route1.append(temp_r11)
        cm2_route2.append(temp_r21)
        cm2_route3.append(temp_r31)
        cm2_route4.append(temp_r41)
        cm2_er1.append(temp_er1)
        cm2_er2.append(temp_er2)
        cm2_er3.append(temp_er3)
        cm2_er4.append(temp_er4)

        # Run the Minority Game algorithm...
        find_winning_action_update_random(sla_iteration)

        print("Updating the SLA corresponding probabilities...")
        #mg_update_actions()
        #reform_mgtable()
        #Check if we have SLA convergence . . .
        sla_count_human = 0
        for i in range(len(set_of_humans)):
            break_temp = 1
            for j in range(len(evacuation_routes)):


                if (action_probability[i][j] >= 0.9):
                    if(break_temp==1):
                        break_temp -= 1
                        sla_count_human = sla_count_human + 1
                    #break
        if (sla_count_human == len(set_of_humans)):
            if (len(action_probability) != 0):
                for u in range(len(action_probability)):
                    max_index = action_probability[u].index(max(action_probability[u]))
                    if (number_of_humans[max_index] != 0):
                        reward_pr2.append(((evacuation_rate[max_index] * ((1.0 * capacity_route[max_index]) / number_of_humans[max_index])) * 1.0) / (distance_from_evac_routes[u][max_index] * ((1.0 * agg_evacuation_rate[max_index] / sla_iteration))))
                    else:
                        reward_pr2.append(((evacuation_rate[max_index] * ((1.0 * capacity_route[max_index]) - number_of_humans[max_index])) * 1.0) / (distance_from_evac_routes[u][max_index] * ((1.0 * agg_evacuation_rate[max_index] / sla_iteration))))

            sla_convergence = 1

            print ("SLA CONVERGENCE RANDOM. . . ")

        else:

            sla_iteration += 1
            iteration_list2.append(sla_iteration)
        print action_probability
        print(len(action_probability))
        print sla_iteration



# -----------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------
# **************************** SLA with distance weights to evacuate or not ********************************
def mg_choose_action_distweight(route):
    for k in range(len(selected_routes)):
        if(selected_routes[k] == route+1):
            temp_list = [1,0]
            weight1 = (1.0/distance_from_evac_routes[k][route])*0.5
            weight2 = 1-weight1
            print("Weight1:"+str(weight1))
            print("Weight2:"+str(weight2))
            chosen_action[k] = numpy.random.choice(temp_list, p=[weight1, weight2])

    #print chosen_action
    return chosen_action

def find_winning_action_update_distweight(sla_iteration):
    conv_counter = 0
    print("MPIKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
    for i in range(len(humans_want_route)):
        convergence = 0
        players = []
        evacuating_users_temp = 0                       # How many will actually evacuate in Route i
        if(number_of_humans[i]<capacity_route[i]):      # Only if this condition applies for the Route, run the Minority Game
            mg_choose_action_distweight(i)
            not_evacuate = 0
            evacuate = 0
            for k in range(len(selected_routes)):                       # Finding the winning action process
                if (selected_routes[k] == i+1):
                    players.append(k)
                    if(chosen_action[k] == 0):                          # Chosen Action : What humank wants to do?
                        not_evacuate += 1
                    else:
                        evacuate += 1


            evacuating_selected = []
            evacuating_users = 0  # Initialize the users that want to evacuate in every different evacuation route
            for q in range(len(selected_routes)):
                if (selected_routes[q] == i + 1):     # Check if the j-th human wants the i-th evacuation route
                    if (chosen_action[q] == 1):       # If the j-th human finally chose the winning action
                        evacuating_users = evacuating_users + 1
                        # print("Evacuating users:"+str(evacuating_users))
                        evacuating_selected.append(q)
                        if (number_of_humans[i] != 0):
                            reward_pr3.append(((evacuation_rate[i] * ((1.0 * capacity_route[i]) / number_of_humans[i])) * 1.0) / (distance_from_evac_routes[q][i] * ((1.0 * agg_evacuation_rate[i] / sla_iteration))))
                        else:
                            reward_pr3.append(((evacuation_rate[i] * ((1.0 * capacity_route[i]) - number_of_humans[i])) * 1.0) / (distance_from_evac_routes[q][i] * ((1.0 * agg_evacuation_rate[i] / sla_iteration))))

            evacuating_users_temp = evacuating_users

            for k in range(len(evacuating_selected)):
                selected_routes[evacuating_selected[k]] = -3
                chosen_action[evacuating_selected[k]] = -3
                set_of_humans[evacuating_selected[k]] = -3
                distance_from_evac_routes[evacuating_selected[k]] = -3
                action_probability[evacuating_selected[k]] = -3
                #mg_table[evacuating_selected[k]] = -3
                norm_rew_prob[evacuating_selected[k]] = -3

            selected_routes[:] = (value for value in selected_routes if value != -3)
            chosen_action[:] = (value for value in chosen_action if value != -3)
            set_of_humans[:] = (value for value in set_of_humans if value != -3)
            distance_from_evac_routes[:] = (value for value in distance_from_evac_routes if value != -3)
            action_probability[:] = (value for value in action_probability if value != -3)
            #mg_table[:] = (value for value in mg_table if value != -3)
            norm_rew_prob[:] = (value for value in norm_rew_prob if value != -3)

        number_of_humans[i] = number_of_humans[i] - int(evacuation_rate[i] * number_of_humans[i]) + evacuating_users_temp

        # We have to update the evacuation rate of each evacuation route

        if(number_of_humans[i]<capacity_route[i]):
            print("AAAAAAAA")

            evacuation_rate[i] = initial_evacuation_rate[i] * ((capacity_route[i] - number_of_humans[i]) / (1.0 * (capacity_route[i])))

        elif(number_of_humans[i]>capacity_route[i]):
            print("BBBBBBBBB")
            evacuation_rate[i] = initial_evacuation_rate[i] *(1.0*capacity_route[i]/number_of_humans[i])

        agg_evacuation_rate[i] = agg_evacuation_rate[i] + evacuation_rate[i]
        #number_of_humans[i] = number_of_humans[i] - int(evacuation_rate[i] * number_of_humans[i]) + evacuating_users_temp


    print("VGIKAAAAAAAAAAAAAAAAAAAAAAAA")
    reward_probability(sla_iteration)
    normalized_reward_probability()

    for o in range(len(set_of_humans)):
        chosen_route = selected_routes[o]
        update_action_probability(o,chosen_route,sla_iteration)

def sla_distweight_choose():
    sla_convergence = 0  # Boolean variable to check if we have an SLA convergence\
    sla_iteration = 1
    iteration_list3.append(1)
    while (sla_convergence != 1):
        print("RAAAAAAANDOOOOOOOOOM")
        choose_evacuation_route()
        print("The selected routes in SLA are:"+str(selected_routes))

        temp_r1 = 0
        temp_r2 = 0
        temp_r3 = 0
        temp_r4 = 0
        temp_er1 = 0
        temp_er2 = 0
        temp_er3 = 0
        temp_er4 = 0
        '''
        for k in range(len(selected_routes)):
            if (selected_routes[k] == 1):
                temp_r1 += 1
            elif (selected_routes[k] == 2):
                temp_r2 += 1
            elif (selected_routes[k] == 3):
                temp_r3 += 1
            else:
                temp_r4 += 1
        '''
        for k in range(len(humans_want_route)):
            if (k == 0):
                temp_r1 = number_of_humans[k]
            elif (k == 1):
                temp_r2 = number_of_humans[k]
            elif (k == 2):
                temp_r3 = number_of_humans[k]
            else:
                temp_r4 = number_of_humans[k]

        for k in range(len(humans_want_route)):
            if (k == 0):
                temp_er1 = evacuation_rate[k]
            elif (k == 1):
                temp_er2 = evacuation_rate[k]
            elif (k == 2):
                temp_er3 = evacuation_rate[k]
            else:
                temp_er4 = evacuation_rate[k]
        cm3_route1.append(temp_r1)
        cm3_route2.append(temp_r2)
        cm3_route3.append(temp_r3)
        cm3_route4.append(temp_r4)
        cm3_er1.append(temp_er1)
        cm3_er2.append(temp_er2)
        cm3_er3.append(temp_er3)
        cm3_er4.append(temp_er4)
        # Run the Minority Game algorithm...
        find_winning_action_update_distweight(sla_iteration)

        print("Updating the SLA corresponding probabilities...")
        #mg_update_actions()
        #reform_mgtable()
        #Check if we have SLA convergence . . .
        sla_count_human = 0
        for i in range(len(set_of_humans)):
            break_temp = 1
            for j in range(len(evacuation_routes)):


                if (action_probability[i][j] >= 0.9):
                    if(break_temp==1):
                        break_temp -= 1
                        sla_count_human = sla_count_human + 1
                    #break
        if (sla_count_human == len(set_of_humans)):
            #print (sla_count_human == len(set_of_humans))
            if (len(action_probability) != 0):
                for u in range(len(action_probability)):
                    max_index = action_probability[u].index(max(action_probability[u]))
                    if (number_of_humans[max_index] != 0):
                        reward_pr3.append(((evacuation_rate[max_index] * ((1.0 * capacity_route[max_index]) / number_of_humans[max_index])) * 1.0) / (distance_from_evac_routes[u][max_index] * ((1.0 * agg_evacuation_rate[max_index] / sla_iteration))))
                    else:
                        reward_pr3.append(((evacuation_rate[max_index] * ((1.0 * capacity_route[max_index]) - number_of_humans[max_index])) * 1.0) / (distance_from_evac_routes[u][max_index] * ((1.0 * agg_evacuation_rate[max_index] / sla_iteration))))

            sla_convergence = 1
            # Find the average Normalized Reward

            print ("SLA CONVERGENCE . . . ")

        else:

            sla_iteration += 1
            iteration_list3.append(sla_iteration)
        print action_probability
        print(len(action_probability))
        print sla_iteration






# ------------------------------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------
# **************************** SLA with Capacity-Humans already in route to evacuate or not ********************************
def mg_choose_action_cap(route):
    for k in range(len(selected_routes)):
        if(selected_routes[k] == route+1):
            temp_list = [0,1]
            if(capacity_route[route]>=number_of_humans[route]):
                if(number_of_humans[route]!=0):
                    weight1 = (number_of_humans[route]*1.0)/capacity_route[route]
                    weight2 = 1.0 - weight1
                else:
                    print("MODA")
                    weight1=0.5
                    weight2=0.5
            else:
                weight2 = (capacity_route[route]*1.0)/number_of_humans[route]
                weight1 = 1.0-weight2
            chosen_action[k] = numpy.random.choice(temp_list, p=[weight1, weight2])

    print("CAP CHOSEN:"+str(chosen_action))
    #print chosen_action
    return chosen_action

def find_winning_action_update_cap(sla_iteration):
    conv_counter = 0
    print("MPIKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
    for i in range(len(humans_want_route)):
        convergence = 0
        players = []
        evacuating_users_temp = 0                       # How many will actually evacuate in Route i
        if(number_of_humans[i]<capacity_route[i]):      # Only if this condition applies for the Route, run the Minority Game
            print("MPIKAAAAA REEEEEEEEE")
            mg_choose_action_cap(i)
            not_evacuate = 0
            evacuate = 0
            for k in range(len(selected_routes)):                       # Finding the winning action process
                if (selected_routes[k] == i+1):
                    players.append(k)
                    if(chosen_action[k] == 0):                          # Chosen Action : What humank wants to do?
                        not_evacuate += 1
                    else:
                        evacuate += 1
            print("CAP EVACUATING"+str(evacuate))

            evacuating_selected = []
            evacuating_users = 0  # Initialize the users that want to evacuate in every different evacuation route
            for q in range(len(selected_routes)):
                if (selected_routes[q] == i + 1):     # Check if the j-th human wants the i-th evacuation route
                    if (chosen_action[q] == 1):       # If the j-th human finally chose the winning action
                        evacuating_users = evacuating_users + 1
                        # print("Evacuating users:"+str(evacuating_users))
                        evacuating_selected.append(q)
                        if (number_of_humans[i] != 0):
                            reward_pr4.append(((evacuation_rate[i] * ((1.0 * capacity_route[i]) / number_of_humans[i])) * 1.0) / (distance_from_evac_routes[q][i] * ((1.0 * agg_evacuation_rate[i] / sla_iteration))))
                        else:
                            reward_pr4.append(((evacuation_rate[i] * ((1.0 * capacity_route[i]) - number_of_humans[i])) * 1.0) / (distance_from_evac_routes[q][i] * ((1.0 * agg_evacuation_rate[i] / sla_iteration))))

            evacuating_users_temp = evacuating_users

            for k in range(len(evacuating_selected)):
                selected_routes[evacuating_selected[k]] = -3
                chosen_action[evacuating_selected[k]] = -3
                set_of_humans[evacuating_selected[k]] = -3
                distance_from_evac_routes[evacuating_selected[k]] = -3
                action_probability[evacuating_selected[k]] = -3
                #mg_table[evacuating_selected[k]] = -3
                norm_rew_prob[evacuating_selected[k]] = -3

            selected_routes[:] = (value for value in selected_routes if value != -3)
            chosen_action[:] = (value for value in chosen_action if value != -3)
            set_of_humans[:] = (value for value in set_of_humans if value != -3)
            distance_from_evac_routes[:] = (value for value in distance_from_evac_routes if value != -3)
            action_probability[:] = (value for value in action_probability if value != -3)
            #mg_table[:] = (value for value in mg_table if value != -3)
            norm_rew_prob[:] = (value for value in norm_rew_prob if value != -3)

        number_of_humans[i] = number_of_humans[i] - int(evacuation_rate[i] * number_of_humans[i]) + evacuating_users_temp

        # We have to update the evacuation rate of each evacuation route

        if(number_of_humans[i]<capacity_route[i]):
            print("AAAAAAAA")

            evacuation_rate[i] = initial_evacuation_rate[i] * ((capacity_route[i] - number_of_humans[i]) / (1.0 * (capacity_route[i])))

        elif(number_of_humans[i]>capacity_route[i]):
            print("BBBBBBBBB")
            evacuation_rate[i] = initial_evacuation_rate[i] *(1.0*capacity_route[i]/number_of_humans[i])

        agg_evacuation_rate[i] = agg_evacuation_rate[i] + evacuation_rate[i]
        #number_of_humans[i] = number_of_humans[i] - int(evacuation_rate[i] * number_of_humans[i]) + evacuating_users_temp


    print("VGIKAAAAAAAAAAAAAAAAAAAAAAAA")
    reward_probability(sla_iteration)
    normalized_reward_probability()

    for o in range(len(set_of_humans)):
        chosen_route = selected_routes[o]
        update_action_probability(o,chosen_route,sla_iteration)

def sla_cap_choose():
    sla_convergence = 0  # Boolean variable to check if we have an SLA convergence\
    sla_iteration = 1
    iteration_list4.append(1)
    while (sla_convergence != 1):
        print("RAAAAAAANDOOOOOOOOOM")
        choose_evacuation_route()
        print("The selected routes in SLA are:"+str(selected_routes))

        temp_r1 = 0
        temp_r2 = 0
        temp_r3 = 0
        temp_r4 = 0
        temp_er1 = 0
        temp_er2 = 0
        temp_er3 = 0
        temp_er4 = 0
        '''
        for k in range(len(selected_routes)):
            if (selected_routes[k] == 1):
                temp_r1 += 1
            elif (selected_routes[k] == 2):
                temp_r2 += 1
            elif (selected_routes[k] == 3):
                temp_r3 += 1
            else:
                temp_r4 += 1
        '''
        for k in range(len(humans_want_route)):
            if (k == 0):
                print("lamda")
                temp_r1 = number_of_humans[k]
                print (str(temp_r1))
            elif (k == 1):
                print("lamda")
                temp_r2 = number_of_humans[k]
                print ("This is TEMP:"+str(temp_r2))
            elif (k == 2):
                print("lamda")
                temp_r3 = number_of_humans[k]
                print temp_r3
            else:
                print("lamda")
                temp_r4 = number_of_humans[k]
        for k in range(len(humans_want_route)):
            if (k == 0):
                temp_er1 = evacuation_rate[k]
            elif (k == 1):
                temp_er2 = evacuation_rate[k]
            elif (k == 2):
                temp_er3 = evacuation_rate[k]
            else:
                temp_er4 = evacuation_rate[k]
        cm4_route1.append(temp_r1)
        cm4_route2.append(temp_r2)
        cm4_route3.append(temp_r3)
        cm4_route4.append(temp_r4)
        cm4_er1.append(temp_er1)
        cm4_er2.append(temp_er2)
        cm4_er3.append(temp_er3)
        cm4_er4.append(temp_er4)
        # Run the Minority Game algorithm...
        find_winning_action_update_cap(sla_iteration)

        print("Updating the SLA corresponding probabilities...")
        #mg_update_actions()
        #reform_mgtable()
        #Check if we have SLA convergence . . .
        sla_count_human = 0
        for i in range(len(set_of_humans)):
            for j in range(len(evacuation_routes)):
                if (action_probability[i][j] >= 0.9):
                    sla_count_human = sla_count_human + 1
                    break
        if (sla_count_human == len(set_of_humans)):
            #print (sla_count_human == len(set_of_humans))
            if (len(action_probability) != 0):
                for u in range(len(action_probability)):
                    max_index = action_probability[u].index(max(action_probability[u]))
                    if (number_of_humans[max_index] != 0):
                        reward_pr4.append(((evacuation_rate[max_index] * ((1.0 * capacity_route[max_index]) / number_of_humans[max_index])) * 1.0) / (distance_from_evac_routes[u][max_index] * ((1.0 * agg_evacuation_rate[max_index] / sla_iteration))))
                    else:
                        reward_pr4.append(((evacuation_rate[max_index] * ((1.0 * capacity_route[max_index]) - number_of_humans[max_index])) * 1.0) / (distance_from_evac_routes[u][max_index] * ((1.0 * agg_evacuation_rate[max_index] / sla_iteration))))

            sla_convergence = 1
            # Find the average Normalized Reward

            print ("SLA CONVERGENCE . . . ")

        else:

            sla_iteration += 1
            iteration_list4.append(sla_iteration)
        print action_probability
        print(len(action_probability))
        print sla_iteration







# ------------------------------------------------------------------------------------------------------------------------------------

temp = 0


cm1_route1 = []
cm1_route2 = []
cm1_route3 = []
cm1_route4 = []
cm2_route1 = []
cm2_route2 = []
cm2_route3 = []
cm2_route4 = []
cm3_route1 = []
cm3_route2 = []
cm3_route3 = []
cm3_route4 = []
cm4_route1 = []
cm4_route2 = []
cm4_route3 = []
cm4_route4 = []
#----------------
cm1_er1 = []
cm1_er2 = []
cm1_er3 = []
cm1_er4 = []
cm2_er1 = []
cm2_er2 = []
cm2_er3 = []
cm2_er4 = []
cm3_er1 = []
cm3_er2 = []
cm3_er3 = []
cm3_er4 = []
cm4_er1 = []
cm4_er2 = []
cm4_er3 = []
cm4_er4 = []
#----------------
reward_pr1 = []
reward_pr2 = []
reward_pr3 = []
reward_pr4 = []
#----------------

iteration_list1 = []
iteration_list2 = []
iteration_list3 = []
iteration_list4 = []


# Run the SLA with the traditional Minority Games
sla()
for i in range(len(evacuation_rate)):
    evacuation_rate[i]=initial_evacuation_rate[i]
norm_rew_prob= []
agg_rew= []
agg_evacuation_rate=[]
initialize_agg_evac_rate()
for i in range(len(evacuation_rate)):
    agg_evacuation_rate[i] += evacuation_rate[i]
action_probability = []
set_of_humans= []
create_setofhumans(331)
selected_routes = [0] * len(set_of_humans)                                    # Represents in which route, each human wants to go; i.e [1,3,2,...] --> The first human in the 1st route, ....
chosen_action = []
chosen_actions()
number_of_humans = []
initialize_humans_in_evac()
distance_from_evac_routes = []
for i in range(len(set_of_humans)):
    distance_from_evac_routes.append([0 for k in range(len(evacuation_routes))])
for i in range(len(set_of_humans)):
    for j in range(len(evacuation_routes)):
        distance_from_evac_routes[i][j] = initial_dist[i][j]
initialize_norm_pr_list()
initialize_action_probability()
initiate_humans_want_route()




# Run the SLA with 50% probability to evacuate and 50%probability not to evacuate
sla_random_choose()
for i in range(len(evacuation_rate)):
    evacuation_rate[i]=initial_evacuation_rate[i]

norm_rew_prob= []
agg_rew= []
agg_evacuation_rate=[]
initialize_agg_evac_rate()
for i in range(len(evacuation_rate)):
    agg_evacuation_rate[i] += evacuation_rate[i]
action_probability = []
set_of_humans= []
create_setofhumans(331)
selected_routes = [0] * len(set_of_humans)                                    # Represents in which route, each human wants to go; i.e [1,3,2,...] --> The first human in the 1st route, ....
chosen_action = []
chosen_actions()
number_of_humans = []
initialize_humans_in_evac()
distance_from_evac_routes = []
for i in range(len(set_of_humans)):
    distance_from_evac_routes.append([0 for k in range(len(evacuation_routes))])
for i in range(len(set_of_humans)):
    for j in range(len(evacuation_routes)):
        distance_from_evac_routes[i][j] = initial_dist[i][j]
initialize_norm_pr_list()
initialize_action_probability()
initiate_humans_want_route()



sla_distweight_choose()

for i in range(len(evacuation_rate)):
    evacuation_rate[i]=initial_evacuation_rate[i]
norm_rew_prob= []
agg_rew= []
agg_evacuation_rate=[]
initialize_agg_evac_rate()
for i in range(len(evacuation_rate)):
    agg_evacuation_rate[i] += evacuation_rate[i]
action_probability = []
set_of_humans= []
create_setofhumans(331)
selected_routes = [0] * len(set_of_humans)                                    # Represents in which route, each human wants to go; i.e [1,3,2,...] --> The first human in the 1st route, ....
chosen_action = []
chosen_actions()
number_of_humans = []
initialize_humans_in_evac()
distance_from_evac_routes = []
for i in range(len(set_of_humans)):
    distance_from_evac_routes.append([0 for k in range(len(evacuation_routes))])
for i in range(len(set_of_humans)):
    for j in range(len(evacuation_routes)):
        distance_from_evac_routes[i][j] = initial_dist[i][j]
initialize_norm_pr_list()
initialize_action_probability()
initiate_humans_want_route()




# Run the SLA with Capacity to evacuate or not
sla_cap_choose()



fig, ax = plt.subplots()
objects = ['Route1','Route2','Route3','Route4']
n_groups = 4  # 4 different methods
bar_width = 0.15
opacity = 0.8
index = numpy.arange(n_groups)

avg_cluster_size_m1 = [sum(cm1_route1)/len(iteration_list1),sum(cm1_route2)/len(iteration_list1),sum(cm1_route3)/len(iteration_list1),sum(cm1_route4)/len(iteration_list1)]
avg_cluster_size_m2 = [sum(cm2_route1)/len(iteration_list2),sum(cm2_route2)/len(iteration_list2),sum(cm2_route3)/len(iteration_list2),sum(cm2_route4)/len(iteration_list2)]
avg_cluster_size_m3 = [sum(cm3_route1)/len(iteration_list3),sum(cm3_route2)/len(iteration_list3),sum(cm3_route3)/len(iteration_list3),sum(cm3_route4)/len(iteration_list3)]
avg_cluster_size_m4 = [sum(cm4_route1)/len(iteration_list4),sum(cm4_route2)/len(iteration_list4),sum(cm4_route3)/len(iteration_list4),sum(cm4_route4)/len(iteration_list4)]



print("ClusterM1:"+str(avg_cluster_size_m1))
print("ClusterM2:"+str(avg_cluster_size_m2))

print("Rroute1:"+str(cm1_route1))
print("Rroute2:"+str(cm1_route2))
print("Rroute3:"+str(cm1_route3))
print("Rroute4:"+str(cm1_route4))
print("---------------------------")
print("Rroute1:"+str(cm2_route1))
print("Rroute2:"+str(cm2_route2))
print("Rroute3:"+str(cm2_route3))
print("Rroute4:"+str(cm2_route4))
print("---------------------------")
print("Rroute1:"+str(cm4_route1))
print("Rroute2:"+str(cm4_route2))
print("Rroute3:"+str(cm4_route3))
print("Rroute4:"+str(cm4_route4))


print("Athroisma1:"+str(sum(cm1_route1)))
print("Athroisma2:"+str(sum(cm2_route1)))
print("IterationsList1:"+str(len(iteration_list1)))
print("IterationsList2:"+str(len(iteration_list2)))
print("Capacity:"+str(capacity_route))
print("Initial Rate:"+str(initial_evacuation_rate))
rects1 = plt.bar(index, avg_cluster_size_m1, bar_width,
                 alpha=opacity,
                 color='b',
                 label='Minority Game')

rects2 = plt.bar(index+bar_width, avg_cluster_size_m2, bar_width,
                 alpha=opacity,
                 color='r',
                 label='Random Choose')


rects3 = plt.bar(index+bar_width+bar_width, avg_cluster_size_m3, bar_width,
                 alpha=opacity,
                 color='yellow',
                 label='Weights')


rects4 = plt.bar(index+bar_width+bar_width+bar_width, avg_cluster_size_m4, bar_width,
                 alpha=opacity,
                 color='green',
                 label='Capacity')

plt.axhline(y=capacity_route[0],color='brown',linestyle='--',label="R1 Cap")
plt.axhline(y=capacity_route[1],color='orange',linestyle='--',label="R2 Cap")
plt.axhline(y=capacity_route[2],color='indigo',linestyle='--',label="R3 Cap")
plt.axhline(y=capacity_route[3],color='black',linestyle='--',label="R4 Cap")


plt.ylim(top=max(capacity_route)+10)
plt.xlabel('Routes')
plt.ylabel('Average Humans Already in Route')
plt.xticks(index + bar_width, ('Route1', 'Route2', 'Route3', 'Route4'))
plt.legend(loc=9, bbox_to_anchor=(0.8, 1.05), ncol=2)
plt.tight_layout()
plt.show(block=True)


#--------------------------------- Bar Plot Evacuation Rates --------------------------------
fig, ax = plt.subplots()
objects = ['Route1','Route2','Route3','Route4']
n_groups = 4  # 4 different methods
bar_width = 0.15
opacity = 0.8
index = numpy.arange(n_groups)

avg_cluster_size_m1 = [sum(cm1_er1)/len(iteration_list1),sum(cm1_er2)/len(iteration_list1),sum(cm1_er3)/len(iteration_list1),sum(cm1_er4)/len(iteration_list1)]
avg_cluster_size_m2 = [sum(cm2_er1)/len(iteration_list2),sum(cm2_er2)/len(iteration_list2),sum(cm2_er3)/len(iteration_list2),sum(cm2_er4)/len(iteration_list2)]
avg_cluster_size_m3 = [sum(cm3_er1)/len(iteration_list3),sum(cm3_er2)/len(iteration_list3),sum(cm3_er3)/len(iteration_list3),sum(cm3_er4)/len(iteration_list3)]
avg_cluster_size_m4 = [sum(cm4_er1)/len(iteration_list4),sum(cm4_er2)/len(iteration_list4),sum(cm4_er3)/len(iteration_list4),sum(cm4_er4)/len(iteration_list4)]

rectss1 = plt.bar(index, avg_cluster_size_m1, bar_width,
                 alpha=opacity,
                 color='b',
                 label='Minority Game')

rectss2 = plt.bar(index+bar_width, avg_cluster_size_m2, bar_width,
                 alpha=opacity,
                 color='r',
                 label='Random Choose')


rectss3 = plt.bar(index+bar_width+bar_width, avg_cluster_size_m3, bar_width,
                 alpha=opacity,
                 color='yellow',
                 label='Weights')


rectss4 = plt.bar(index+bar_width+bar_width+bar_width, avg_cluster_size_m4, bar_width,
                 alpha=opacity,
                 color='green',
                 label='Capacity')


plt.xlabel('Routes')
plt.ylabel('Average Evacuation Rates')
plt.xticks(index + bar_width, ('Route1', 'Route2', 'Route3', 'Route4'))
plt.legend(loc=9, bbox_to_anchor=(0.8, 1.05), ncol=2)
plt.tight_layout()
plt.show(block=True)

#-------------------------------------------------------------------------

objects = ('Our Approach', 'Method2', 'Method3', 'Method4')
y_pos=numpy.arange(len(objects))
print("LLLLENGTH:"+str(len(reward_pr1)))
y = [sum(reward_pr1)/331.0,sum(reward_pr2)/331.0,sum(reward_pr3)/331.0,sum(reward_pr4)/331.0]
plt.bar(y_pos, y, align='center', alpha=0.2)
plt.xticks(y_pos, objects)
plt.ylabel('Average Reward Probability')
plt.show(block=True)



print(str(sum(reward_pr1)/331))
print(str(sum(reward_pr2)/331))
print(str(sum(reward_pr3)/331))
print(str(sum(reward_pr4)/331))

