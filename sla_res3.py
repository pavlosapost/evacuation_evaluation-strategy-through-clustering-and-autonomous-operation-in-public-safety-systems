import math
import random
from random import randint
import numpy
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties

evacuation_routes = []                                              # Evacuation Routes
set_of_humans = []                                                  # Set of Humans
capacity_route = []                                                 # Capacity of each Evacuation Route
evacuation_rate = []                                                # Evacuation Rate of each Evacuation Route
initial_evacuation_rate = []                                        # Needed for the MG update of the evacuation rate
agg_evacuation_rate = []                                            # Aggrandize the evacuation rates of all iterations
number_of_humans = []                                               # Number of humans already in Evacuation Route E(i)
distance_from_evac_routes = []                                      # Distance of human m from evacuation route e (2dimensional)
norm_rew_prob = []                                                  # Normalized Reward Probability
sla_iteration = 1

action_probability = []
mg_table = []                                                       # Minority games Table
mg_minority_table = []                                               # Minority Games Winning Actionv Table

mg_g = 0.8                                                        # Minority Game parameter learning rate - Small: Check the alternatives and not overflow
#mg_evacuating_users = 0                                             # MG_Users who actually evacuate

# ******* Stochastic Learning Automata Algorithm description ************
#   1. SLA algorithm iterates in mixed strategy forms [ = Probabilistic Vectors ]
#   2. In the SLA algorithm, the game is played once in a slot according to the mixed strategy profile of the players
#   3. After each play, each player receives a payoff Rn(k) [kth slot] and updates its mixed strategy based on the received payoff --> Linear Reward Inaction
#   4. In particular if an action is selected and a postive payoff is received, the probability of choosing this action in the next slot increases.

#   Internal State of the Agent:
#   Probability distribution according to which actions would be chosen.
#   The probabilities of taking different actions would be adjusted according to
#   their previous successes and failures.

# ***********************************************************************


print("\n")

def create_evac_routes(number):
    for i in range(number) :
        evacuation_routes.append(i+1)                        # [evac0, evac1, evac2, ...]
    return evacuation_routes
print ("The available evacuation routes are the following:")
print create_evac_routes(4)
print ("\n")



def create_setofhumans(number):
    for i in range(number) :
        set_of_humans.append("human"+str(i+1))                            # [h0, h1, h2, ...]
    return set_of_humans
print ("The humans ready for evacuation are the following:")
print create_setofhumans(101)
print ("\n")



def initialize_capacity() :                                                 # [10, 23, 54, ...] , capacity of each evacuation route
    length_cap_routes = len(evacuation_routes)
    for i in range(length_cap_routes) :
        capacity_route.append(randint(9,15))
    return capacity_route
print ("The corresponding capacity routes are the following:")
print initialize_capacity()
print ("\n")


def initialize_agg_evac_rate():
    length_cap_routes = len(evacuation_routes)
    for i in range(length_cap_routes):
        agg_evacuation_rate.append(0)                                       # Initialization of aggrandized evacuation rates,
    return agg_evacuation_rate                                              # Needed for the Reward Probability denominator





def initialize_evac_rate():                                                 # [0.123232, 0.54432, 0.45676, ...]
    length_cap_routes = len(evacuation_routes)
    for i in range(length_cap_routes) :
        evacuation_rate.append(random.uniform(0.3, 0.9))                             # Produce a random float rate
        initial_evacuation_rate.append(0)
        initial_evacuation_rate[i] = evacuation_rate[i]
        agg_evacuation_rate[i] = (agg_evacuation_rate[i]+evacuation_rate[i])
    return evacuation_rate
initialize_agg_evac_rate()
print ("The evacuation rate is the following:")
print initialize_evac_rate()

print ("The aggrindezed evacuation rate is the following (Needed for the Reward Probability):")
print agg_evacuation_rate
print("\n")


def initialize_humans_in_evac() :
    length_cap_routes = len(evacuation_routes)
    for i in range(length_cap_routes):
        humans_in_route_i = 0                                                # Initialization with 0. No human in any evacuation route
        number_of_humans.append(humans_in_route_i)
    return number_of_humans
print ("Number of humans in each evacuation route initialization:")
print initialize_humans_in_evac()
print("\n")



def initialize_distance() :
    for j in range(len(set_of_humans)):
        distance_from_evac_routes.append([randint(10,20) for i in range(len(evacuation_routes))])     # [[3,4,6], [45,23,1], ...]
    return distance_from_evac_routes                                                                 # The first element monitors the distances of the first human from all evacuation routes

print("The distance of each human from each and every evacuation route :")
print initialize_distance()
print("\n")




def initialize_norm_pr_list() :
    for i in range(len(set_of_humans)):
        norm_rew_prob.append([0.0 for j in range(len(evacuation_routes))])
    print("Initialization")
    print norm_rew_prob
    return norm_rew_prob

initialize_norm_pr_list()


# For all the evacuation routes, find all the probability rewards for each human
def reward_probability() :
    for i in range(len(set_of_humans)):
        for j in range(len(evacuation_routes)):
            norm_rew_prob[i][j] = ((evacuation_rate[j]*((1.0*capacity_route[j])/number_of_humans[j]))*1.0)/(distance_from_evac_routes[i][j] *((1.0*agg_evacuation_rate[j]/sla_iteration)))
    return norm_rew_prob
    # Normalization process TODO: Ask about the denominator of the normalized probability

def normalized_reward_probability() :
    for j in range(len(set_of_humans)):
        agg = 0.0 # needed local variable needed for the normalization process --> Denominator
        for i in range(len(evacuation_routes)):
            agg = agg + norm_rew_prob[j][i]
        for k in range(len(evacuation_routes)):
            norm_rew_prob[j][k] = (1.0*norm_rew_prob[j][k])/agg
    print("The normalized Reward Probability of the each human is the following one:")
    print (norm_rew_prob)
    print("\n")
    return norm_rew_prob

#normalized_reward_probability()




# --- Action Probability ---
prob_step = 0.8                        # Step for the Action Probability Adjustment - Could be used as metric

def initialize_action_probability() :       # The initialization creates the same possibility for each and every evacuation route per human
    for i in range(len(set_of_humans)):     # Form : [ human1[route1,route2,...] , human2[route1,route2,...] , ....]
        action_probability.append([1.0/len(evacuation_routes) for j in range(len(evacuation_routes))])
    return action_probability

initialize_action_probability()
print("Initialization of Action Probability - The same for all humans in the 1st SLA Iteration:")
print(action_probability)
print("\n")


def update_action_probability(human, chosen_route) :
    for j in range(len(evacuation_routes)):
        if(evacuation_routes[j] == chosen_route):
            action_probability[human][j] = action_probability[human][j] + prob_step*norm_rew_prob[human][j]*(1-action_probability[human][j])
        else:
            action_probability[human][j] = action_probability[human][j] - prob_step*norm_rew_prob[human][j]*action_probability[human][j]
    action_probability[human] = (action_probability[human]/(numpy.sum(action_probability[human]))).tolist()





# TODO: Here we will have the chosen action of each player and the reward probability. For each human we will update the mixed strategy
# After the action probability vector is updated, the user will choose the evacuation route for the next iteration
# def update_action_probability(TODO:We will have parameters) :   --> This function will operate for EACH human...



route_list = []
def initialize_list_route():
    for i in range(len(evacuation_routes)):
        route_list.append(i+1)
    return route_list

initialize_list_route()
humans_want_route = [0] * len(evacuation_routes)                              # Represents how many humans want to go to each evacuation route respectively
mg_minority_table = [0] * len(evacuation_routes)
def initiate_humans_want_route():
    for i in range(len(evacuation_routes)):
        humans_want_route[i] = 0
        mg_minority_table[i] = 0
    return humans_want_route

selected_routes = [0] * len(set_of_humans)                                    # Represents in which route, each human wants to go; i.e [1,3,2,...] --> The first human in the 1st route, ....
def choose_evacuation_route():                                                # Each human chooses the route with the greatest mixed strategy
    initiate_humans_want_route()
    for i in range(len(set_of_humans)):
        selected_routes[i] = numpy.random.choice(route_list,p=action_probability[i])
        humans_want_route[selected_routes[i]-1] = humans_want_route[selected_routes[i]-1] + 1
    return selected_routes




# ************* Minority Games - Distributed Decision Making ************
# TODO: Remember! If I need which evacuation route chose a human I have already created such a list! --> selected_routes[]
# MG : Decides  who will go where

# Possible actions a(m) = 0 --> do not evacuate
#                  a(m) = 1 --> evacuate

chosen_action = []


def chosen_actions() :
    for k in range(len(set_of_humans)):
        chosen_action.append(-2)
    return chosen_action
chosen_actions()


def initialize_mgtable() :
    for i in range(len(set_of_humans)):
        mg_table.append([0 for j in range(6)])         # Dimensions human*6
        mg_table[i][0] = 0              # Do NOT evacuate!
        mg_table[i][1] = 0.5            # Initialize with 0.5
        mg_table[i][2] = 0
        mg_table[i][3] = 1              # Evacuate!
        mg_table[i][4] = 0.5            # Initialize with 0.5
        mg_table[i][5] = 0
    return mg_table
initialize_mgtable()

def reform_mgtable():
    for i in range(len(set_of_humans)):
        mg_table[i][0] = 0  # Do NOT evacuate!
        mg_table[i][1] = 0.5  # Initialize with 0.5
        mg_table[i][2] = 0
        mg_table[i][3] = 1  # Evacuate!
        mg_table[i][4] = 0.5  # Initialize with 0.5
        mg_table[i][5] = 0
    return mg_table


def mg_choose_action(route):
    for k in range(len(selected_routes)):
        if(selected_routes[k] == route+1):
            temp_list = [mg_table[k][0],mg_table[k][3]]
            chosen_action[k] = numpy.random.choice(temp_list, p=[mg_table[k][1], mg_table[k][4]])  # Choose by weighting the probabilities

    #print chosen_action
    return chosen_action

# The table that defines the chosen action of each human has now been created:
# [ h1(0) , h2(0) , h3(1) , ... ]
# The first human wants not to evacuate, the second the same, the third wants to evacuate, ...

# The next step is to find the winning action and to update the users' probabilities

def find_winning_action_update_versiontwo():
    conv_counter = 0

    for b in range(len(humans_want_route)):
        if (number_of_humans[b] < capacity_route[b]):
            conv_counter += 1

    while(conv_counter!=0):
        for i in range(len(humans_want_route)):
            convergence = 0
            players = []
            mg_iteration = 1
            evacuating_users_temp = 0                       # How many will actually evacuate in Route i
            if(number_of_humans[i]<capacity_route[i]):      # Only if this condition applies for the Route, run the Minority Game
                while(convergence != 1):
                    mg_choose_action(i)
                    not_evacuate = 0
                    evacuate = 0
                    for k in range(len(selected_routes)):                       # Finding the winning action process
                        if (selected_routes[k] == i+1):
                            players.append(k)
                            if(chosen_action[k] == 0):                          # Chosen Action : What humank wants to do?
                                not_evacuate += 1
                            else:
                                evacuate += 1

                    if (evacuate + number_of_humans[i] > capacity_route[i]):  # Define which is the winning action, according to the capacity threshold
                        minority_action = 0
                        mg_minority_table[i] = minority_action  # Update the winning action for each route
                    else:
                        minority_action = 1
                        mg_minority_table[i] = minority_action  # Update the winning action for each route

                    for l in range(len(chosen_action)):      # Update the Probabilities
                        if(selected_routes[l] == i+1):
                            if(chosen_action[l] == minority_action):     # Each human updates its accumulated score
                                if (minority_action == 0):
                                    mg_table[l][2] += 1
                                elif (minority_action == 1):
                                    mg_table[l][5] += 1

                    for p in range(len(selected_routes)):
                        if (selected_routes[p] == i + 1):
                            denominator = math.exp(mg_g * mg_table[p][2]) + math.exp(mg_g * mg_table[p][5])
                            mg_table[p][1] = math.exp(mg_g * mg_table[p][2]) / (1.0 * denominator)
                            mg_table[p][4] = math.exp(mg_g * mg_table[p][5]) / (1.0 * denominator)

                    mg_count = 0
                    for p in range(len(selected_routes)):
                        if (selected_routes[p] == i + 1):

                            if mg_table[p][1] < 0.01:
                                mg_table[p][1] = 0.01
                            if mg_table[p][4] < 0.01:
                                mg_table[p][4] = 0.01
                            if mg_table[p][1] > 0.99:
                                mg_table[p][1] = 0.99
                            if mg_table[p][4] > 0.99:
                                mg_table[p][4] = 0.99

                    for p in range(len(players)):
                        if (abs(mg_table[players[p]][1] - 1) < 0.2 or abs(mg_table[players[p]][4] - 1) < 0.2):
                            mg_count += 1

                    if (mg_count == len(players)):
                        convergence = 1
                        conv_counter -= 1
                        print ("MG TABLE:"+str(mg_table))
                        print("Evacuating & thres_Convergence:"+str(evacuating_users_temp)+str(":::")+str(capacity_route[i]-number_of_humans[i]))
                        print("Minority Game is converged...")
                    else:
                        mg_iteration += 1
                        print("Evacuating & thres_NOTConvergence:"+str(evacuating_users_temp)+str(":::")+str(capacity_route[i]-number_of_humans[i]))

                evacuating_selected = []
                evacuating_users = 0  # Initialize the users that want to evacuate in every different evacuation route
                for q in range(len(selected_routes)):
                    if (selected_routes[q] == i + 1):     # Check if the j-th human wants the i-th evacuation route
                        if (chosen_action[q] == 1):       # If the j-th human finally chose the winning action
                            evacuating_users = evacuating_users + 1
                            # print("Evacuating users:"+str(evacuating_users))
                            evacuating_selected.append(q)

                evacuating_users_temp = evacuating_users

            number_of_humans[i] = number_of_humans[i] - int(evacuation_rate[i] * number_of_humans[i]) + evacuating_users_temp

            # We have to update the evacuation rate of each evacuation route

            if(number_of_humans[i]<capacity_route[i]):

                evacuation_rate[i] = initial_evacuation_rate[i] * ((capacity_route[i] - number_of_humans[i]) / (1.0 * (capacity_route[i])))
            elif(number_of_humans[i]>capacity_route[i]):
                evacuation_rate[i] = initial_evacuation_rate[i] *(1.0*capacity_route[i]/number_of_humans[i])
            agg_evacuation_rate[i] = agg_evacuation_rate[i] + evacuation_rate[i]



    reward_probability()
    normalized_reward_probability()

    for o in range(len(set_of_humans)):
        chosen_route = selected_routes[o]
        update_action_probability(o,chosen_route)





def mg_update_actions() :
    evacuating_selected = []
    for i in range(len(humans_want_route)):
        evacuating_users = 0                     # Initialize the users that want to evacuate in every different evacuation route
        acc_temp = humans_want_route[i]             # Check how many humans want each evacuation route

        if(number_of_humans[i]<capacity_route[i]):
            for j in range(len(selected_routes)):
                if(selected_routes[j]==i+1 ):            # Check if the j-th human wants the i-th evacuation route
                    if (chosen_action[j] == 1) :           # If the j-th human finally chose the winning action
                        # Here we should put out this human from all the tables and to update the mg humans that are evacuating.
                        evacuating_users = evacuating_users + 1
                        #print("Evacuating users:"+str(evacuating_users))
                        evacuating_selected.append(j)


        # ---------------------------------------- Minority Games Updates -----------------------------------------
        # We have to update the number of humans in each evacuation route after the minority game
        number_of_humans[i] = number_of_humans[i] - int(evacuation_rate[i]*number_of_humans[i]) + evacuating_users  # int(float x) --> Round down to nearest integer

        # We have to update the evacuation rate of each evacuation route
        evacuation_rate[i] = initial_evacuation_rate[i]*((capacity_route[i]-number_of_humans[i])/(1.0*(capacity_route[i])))
        agg_evacuation_rate[i] = agg_evacuation_rate[i] + evacuation_rate[i]



        # ----------------------------------------------------------------------------------------------------------


    '''
    # ------ Process in order to extract from all the lists the humans that decided to evacuate ------
    for k in range(len(evacuating_selected)):
        selected_routes[evacuating_selected[k]] = -1
        chosen_action[evacuating_selected[k]] = -1
        set_of_humans[evacuating_selected[k]] = -1
        distance_from_evac_routes[evacuating_selected[k]] = -1
        action_probability[evacuating_selected[k]] = -1
        mg_table[evacuating_selected[k]] = -1
        norm_rew_prob[evacuating_selected[k]] = -1



    # Slicing in order to remove without the need to build a new python object
    selected_routes[:] = (value for value in selected_routes if value != -1)
    chosen_action[:] = (value for value in chosen_action if value != -1)
    set_of_humans[:] = (value for value in set_of_humans if value != -1)
    distance_from_evac_routes[:] = (value for value in distance_from_evac_routes if value != -1)
    action_probability[:] = (value for value in action_probability if value != -1)
    mg_table[:] = (value for value in mg_table if value != -1)
    norm_rew_prob[:] = (value for value in norm_rew_prob if value != -1)
    '''

    # -------------------  Finding the SLA Normalized Reward Probability  ----------------------
    # SOS NOTE : Beware! We should define enough routes in order not to Ce = |M|e and not to Reward_Probability = 0
    reward_probability()
    normalized_reward_probability()                 # Find the normalized Reward Probability for the remaining humans


    # ------------------- Update the SLA Action Probabilities -------------------------
    for q in range(len(set_of_humans)):
        chosen_route = selected_routes[q]
        update_action_probability(q,chosen_route)




print("*********************************************************")
#mg_update_actions()





#------------------------------------- Overall Algorithm Orchestrator ----------------------------------------------------------
sla_convergence = 0                             # Boolean variable to check if we have an SLA convergence
route1 = []
route2 = []
route3 = []
route4 = []




iteration_list = []
iteration_list.append(1)
while (sla_convergence != 1):

    choose_evacuation_route()
    print("The selected routes in SLA are:"+str(selected_routes))

    # Run the Minority Game algorithm...
    find_winning_action_update_versiontwo()
    print("Minority Game final probability array:")
    print mg_table
    print("\n")


    print("Updating the SLA corresponding probabilities...")
    #mg_update_actions()
    reform_mgtable()
    #Check if we have SLA convergence . . .
    sla_count_human = 0
    route1_human=0
    route2_human=0
    route3_human=0
    route4_human=0
    for i in range(len(set_of_humans)):
        break_temp = 1
        for j in range(len(evacuation_routes)):
            if (selected_routes[i] == j+1):
                if(j+1==1):
                    route1_human += 1
                elif (j + 1 == 2):
                    route2_human += 1
                elif (j + 1 == 3):
                    route3_human += 1
                else:
                    route4_human += 1


            if (action_probability[i][j] >= 0.9):
                    if(break_temp==1):
                        break_temp -= 1
                        sla_count_human = sla_count_human + 1
                #break
    route1.append(route1_human)
    route2.append(route2_human)
    route3.append(route3_human)
    route4.append(route4_human)

    if (sla_count_human == len(set_of_humans)):
        #print (sla_count_human == len(set_of_humans))
        sla_convergence = 1
        print ("SLA CONVERGENCE . . . ")

        print("CAPACITY:"+str(capacity_route))
        print("ITERATIO LIST:"+str(len(iteration_list)))

        plt.plot(iteration_list, route1, marker='o', label="R1:C1,AER1:"+str(capacity_route[0])+","+str(round(agg_evacuation_rate[0]/sla_iteration,4)))
        plt.plot(iteration_list, route2, marker='o', label="R2:C2,AER2:"+str(capacity_route[1])+","+str(round(agg_evacuation_rate[1]/sla_iteration,4)))
        plt.plot(iteration_list, route3, marker='o', label="R3:C3,AER3:"+str(capacity_route[2])+","+str(round(agg_evacuation_rate[2]/sla_iteration,4)))
        plt.plot(iteration_list, route4, marker='o', label="R4:C4,AER4:"+str(capacity_route[3])+","+str(round(agg_evacuation_rate[3]/sla_iteration,4)))
        plt.legend(loc='upper center', bbox_to_anchor=(0.99, 0.1),shadow=True, ncol=1)


        fontP = FontProperties()
        fontP.set_size('small')
        #plt.axis([1, len(iteration_list), 0, 1])
        plt.xlabel('SLA Iterations', fontsize=14)
        plt.ylabel('Cluster of humans', fontsize=14)
        plt.title("SLA Humans-Routes Choice")
        plt.show(block=True)
    else:
        sla_iteration += 1
        iteration_list.append(sla_iteration)
    print action_probability
    print(len(action_probability))
    print sla_iteration





# ------------------------------------------------------------------------------------------------------------------------------------

