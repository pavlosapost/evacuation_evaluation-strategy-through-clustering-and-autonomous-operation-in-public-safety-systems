from random import randint
import numpy
import math
import matplotlib.pyplot as plt


evacuation_routes = []                                              # Evacuation Routes
set_of_humans = []
number_of_humans = []                                               # Number of humans already in Evacuation Route E(i)
mg_table = []
chosen_action = []
capacity_route = []                                                 # Capacity of each Evacuation Route
mg_minority_table = []                                               # Minority Games Winning Actionv Table

# Minority Game Learning until 7.. Then we have overflow
#mg_g = 1.2                                                    # Minority Game parameter learning rate - Small: Check the alternatives and not overflow


def create_evac_routes(number):
    for i in range(number) :
        evacuation_routes.append(i+1)                        # [evac0, evac1, evac2, ...]
    return evacuation_routes
print ("The available evacuation routes are the following:")
print create_evac_routes(3)
print ("\n")


def create_setofhumans(number):
    for i in range(number) :
        set_of_humans.append("human"+str(i+1))                            # [h0, h1, h2, ...]
    return set_of_humans
print ("The humans ready for evacuation are the following:")
print create_setofhumans(301)
print ("\n")

def initialize_humans_in_evac() :
    length_cap_routes = len(evacuation_routes)
    for i in range(length_cap_routes):
        humans_in_route_i =randint(1,9)                                                # Initialization with 0. No human in any evacuation route
        number_of_humans.append(humans_in_route_i)
    return number_of_humans
print ("Number of humans in each evacuation route initialization:")
print initialize_humans_in_evac()
print("\n")


def initialize_mgtable() :
    for i in range(len(set_of_humans)):
        mg_table.append([0 for j in range(6)])         # Dimensions human*6
        mg_table[i][0] = 0              # Do NOT evacuate!
        mg_table[i][1] = 0.5            # Initialize with 0.5
        mg_table[i][2] = 0
        mg_table[i][3] = 1              # Evacuate!
        mg_table[i][4] = 0.5            # Initialize with 0.5
        mg_table[i][5] = 0
    return mg_table
initialize_mgtable()



def chosen_actions() :
    for k in range(len(set_of_humans)):
        chosen_action.append(-2)
    return chosen_action
chosen_actions()


def mg_choose_action(route):
    for k in range(len(selected_routes)):
        if(selected_routes[k] == route+1):
            temp_list = [mg_table[k][0],mg_table[k][3]]
            chosen_action[k] = numpy.random.choice(temp_list, p=[mg_table[k][1], mg_table[k][4]])  # Choose by weighting the probabilities

    #print chosen_action
    return chosen_action



selected_routes = [0] * len(set_of_humans)                                    # Represents in which route, each human wants to go; i.e [1,3,2,...] --> The first human in the 1st route, ....
mg_minority_table = [0] * len(evacuation_routes)
def choose_routes():
    for i in range(len(set_of_humans)):
        selected_routes[i] = randint(1,3)
    return selected_routes
choose_routes()

def mg_choose_action(route):
    for k in range(len(selected_routes)):
        if(selected_routes[k] == route+1):
            temp_list = [mg_table[k][0],mg_table[k][3]]
            chosen_action[k] = numpy.random.choice(temp_list, p=[mg_table[k][1], mg_table[k][4]])  # Choose by weighting the probabilities



def initialize_capacity() :                                                 # [10, 23, 54, ...] , capacity of each evacuation route
    length_cap_routes = len(evacuation_routes)
    for i in range(length_cap_routes) :
        capacity_route.append(randint(10,15))
    return capacity_route
print ("The corresponding capacity routes are the following:")
print initialize_capacity()
print ("\n")


def minority_games(mg_g) :
    convergence = 0
    i = 2
    mg_iteration = 1
    players = []
    iteration_list = []
    iteration_list.append(1)
    while (convergence != 1):
        temp = 1
        mg_choose_action(i)
        not_evacuate = 0
        evacuate = 0
        for k in range(len(selected_routes)):  # Finding the winning action process
            if (selected_routes[k] == i + 1):
                players.append(k)
                if (chosen_action[k] == 0):  # Chosen Action : What humank wants to do?
                    not_evacuate += 1
                else:
                    evacuate += 1

        evacuating_users_temp = evacuate
        if (evacuate + number_of_humans[i] > capacity_route[i]):  # Define which is the winning action, according to the capacity threshold
            minority_action = 0
            mg_minority_table[i] = minority_action  # Update the winning action for each route
        else:
            minority_action = 1
            mg_minority_table[i] = minority_action  # Update the winning action for each route

        for l in range(len(chosen_action)):  # Update the Probabilities
            if (selected_routes[l] == i + 1):
                if (chosen_action[l] == minority_action):  # Each human updates its accumulated score
                    if (minority_action == 0):
                        mg_table[l][2] += 1
                    elif (minority_action == 1):
                        mg_table[l][5] += 1

        for p in range(len(selected_routes)):
            if (selected_routes[p] == i + 1):
                denominator = math.exp(mg_g * mg_table[p][2]) + math.exp(mg_g * mg_table[p][5])
                mg_table[p][1] = math.exp(mg_g * mg_table[p][2]) / (1.0 * denominator)
                mg_table[p][4] = math.exp(mg_g * mg_table[p][5]) / (1.0 * denominator)

        mg_count = 0
        for p in range(len(selected_routes)):
            if (selected_routes[p] == i + 1):

                if mg_table[p][1] < 0.01:
                    mg_table[p][1] = 0.01
                if mg_table[p][4] < 0.01:
                    mg_table[p][4] = 0.01
                if mg_table[p][1] > 0.99:
                    mg_table[p][1] = 0.99
                if mg_table[p][4] > 0.99:
                    mg_table[p][4] = 0.99

        for p in range(len(players)):
            if (abs(mg_table[players[p]][1] - 1) < 0.1 or abs(mg_table[players[p]][4] - 1) < 0.1):
                mg_count += 1

        if (mg_count == len(players)):
            convergence = 1
            evacuating_users.append(abs(evacuating_users_temp-(capacity_route[i]-number_of_humans[i])))
            print ("REES:"+str(abs(evacuating_users_temp-(capacity_route[i]-number_of_humans[i]))))

            print("Minority Game is converged...")
            #plt.xticks(iteration_list)
            #plt.plot(iteration_list,choice0_list,marker='d')
            #plt.plot(iteration_list, choice1_list,marker='d')
            #plt.legend(["Choice 0","Choice 1"], loc='upper right')
            #plt.axis([1,len(iteration_list),0,1])
            #plt.xlabel('Iterations', fontsize=14)
            #plt.ylabel('Probabilities', fontsize=14)
            #plt.title("Minority Game choices Convergence")
            #plt.show(block=True)
        else:
            mg_iteration += 1
    #print mg_iteration
    return mg_iteration

conv_list=[]
evac_list = []
mg_step = []
i = 0.5
evacuating_users = []
while i<=7:
    mg_step.append(i)
    agg = 0
    iterations = 0
    evacuating_users = []
    for j in range (100):
        print j
        iterations = minority_games(i)
        agg = agg+iterations
        initialize_mgtable()
    print ("EVAAAAC:"+str(evacuating_users))
    print("EVVVVAC"+str(len(evacuating_users)))
    evac_list.append(sum(evacuating_users)/len(evacuating_users))
    conv_list.append(int(agg/100))
    i += 0.5



fig, ax1 = plt.subplots()
ax1.plot(mg_step, conv_list, marker='d')
ax1.set_xlabel('Step', fontsize=14)
ax1.set_ylabel('Convergence Iterations', color='b')
ax1.tick_params('y', colors='b')
#ax2 = ax1.twinx()
#ax2.plot(mg_step, evac_list, marker='^',color='r')
#ax2.set_ylabel('|Evacuating-(Ce-Me)|', color='r')
#ax2.tick_params('y', colors='r')

plt.title("Minority Learning Step Evaluation")
#fig.tight_layout()
plt.show(block=True)
