from random import randint
import numpy
import math
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties


evacuation_routes = []                                              # Evacuation Routes
set_of_humans = []
number_of_humans = []                                               # Number of humans already in Evacuation Route E(i)
mg_table = []
chosen_action = []
capacity_route = []                                                 # Capacity of each Evacuation Route
mg_minority_table = []                                               # Minority Games Winning Actionv Table

mg_g = 0.9                                                       # Minority Game parameter learning rate - Small: Check the alternatives and not overflow


def create_evac_routes(number):
    for i in range(number) :
        evacuation_routes.append(i+1)                        # [evac0, evac1, evac2, ...]
    return evacuation_routes
print ("The available evacuation routes are the following:")
print create_evac_routes(4)
print ("\n")


def create_setofhumans(number):
    for i in range(number) :
        set_of_humans.append("human"+str(i+1))                            # [h0, h1, h2, ...]
    return set_of_humans
print ("The humans ready for evacuation are the following:")
print create_setofhumans(101)
print ("\n")

def initialize_humans_in_evac() :
    length_cap_routes = len(evacuation_routes)
    for i in range(length_cap_routes):
        humans_in_route_i = 0                                                # Initialization with 0. No human in any evacuation route
        number_of_humans.append(humans_in_route_i)
    return number_of_humans
print ("Number of humans in each evacuation route initialization:")
print initialize_humans_in_evac()
print("\n")


def initialize_mgtable() :
    for i in range(len(set_of_humans)):
        mg_table.append([0 for j in range(6)])         # Dimensions human*6
        mg_table[i][0] = 0              # Do NOT evacuate!
        mg_table[i][1] = 0.5            # Initialize with 0.5
        mg_table[i][2] = 0
        mg_table[i][3] = 1              # Evacuate!
        mg_table[i][4] = 0.5            # Initialize with 0.5
        mg_table[i][5] = 0
    return mg_table
initialize_mgtable()



def chosen_actions() :
    for k in range(len(set_of_humans)):
        chosen_action.append(-2)
    return chosen_action
chosen_actions()


def mg_choose_action(route):
    for k in range(len(selected_routes)):
        if(selected_routes[k] == route+1):
            temp_list = [mg_table[k][0],mg_table[k][3]]
            chosen_action[k] = numpy.random.choice(temp_list, p=[mg_table[k][1], mg_table[k][4]])  # Choose by weighting the probabilities

    #print chosen_action
    return chosen_action


def initialize_humans_in_evac() :
    length_cap_routes = len(evacuation_routes)
    for i in range(length_cap_routes):
        humans_in_route_i = 0                                                # Initialization with 0. No human in any evacuation route
        number_of_humans.append(humans_in_route_i)
    return number_of_humans
print ("Number of humans in each evacuation route initialization:")
print initialize_humans_in_evac()
print("\n")

selected_routes = [0] * len(set_of_humans)                                    # Represents in which route, each human wants to go; i.e [1,3,2,...] --> The first human in the 1st route, ....
mg_minority_table = [0] * len(evacuation_routes)
def choose_routes():
    for i in range(len(set_of_humans)):
        selected_routes[i] = randint(1,4)
    return selected_routes
choose_routes()

def mg_choose_action(route):
    for k in range(len(selected_routes)):
        if(selected_routes[k] == route+1):
            temp_list = [mg_table[k][0],mg_table[k][3]]
            chosen_action[k] = numpy.random.choice(temp_list, p=[mg_table[k][1], mg_table[k][4]])  # Choose by weighting the probabilities



def initialize_capacity() :                                                 # [10, 23, 54, ...] , capacity of each evacuation route
    length_cap_routes = len(evacuation_routes)
    for i in range(length_cap_routes) :
        capacity_route.append(randint(10,15))
    return capacity_route
print ("The corresponding capacity routes are the following:")
print initialize_capacity()
print ("\n")


def minority_games() :
    convergence = 0
    i = 2
    mg_iteration = 1
    evacuating_users_temp = 0  # How many will actually evacuate in Route i
    players = []
    choice0user1_list = []
    choice1user1_list = []
    choice0user2_list = []
    choice1user2_list = []
    choice0user1_list.append(0.5)
    choice1user1_list.append(0.5)
    choice0user2_list.append(0.5)
    choice1user2_list.append(0.5)
    iteration_list = []
    iteration_list.append(1)
    while (convergence != 1):
        temp = 2
        print("MPIKAAAA")
        mg_choose_action(i)
        not_evacuate = 0
        evacuate = 0
        for k in range(len(selected_routes)):  # Finding the winning action process
            if (selected_routes[k] == i + 1):
                players.append(k)
                if (chosen_action[k] == 0):  # Chosen Action : What humank wants to do?
                    not_evacuate += 1
                else:
                    evacuate += 1

        if (evacuate + number_of_humans[i] > capacity_route[i]):  # Define which is the winning action, according to the capacity threshold
            minority_action = 0
            mg_minority_table[i] = minority_action  # Update the winning action for each route
        else:
            minority_action = 1
            mg_minority_table[i] = minority_action  # Update the winning action for each route

        for l in range(len(chosen_action)):  # Update the Probabilities
            if (selected_routes[l] == i + 1):
                if (chosen_action[l] == minority_action):  # Each human updates its accumulated score
                    if (minority_action == 0):
                        mg_table[l][2] += 1
                    elif (minority_action == 1):
                        mg_table[l][5] += 1

        for p in range(len(selected_routes)):
            if (selected_routes[p] == i + 1):
                temp -= 1                       # Graph only one user
                denominator = math.exp(mg_g * mg_table[p][2]) + math.exp(mg_g * mg_table[p][5])
                mg_table[p][1] = math.exp(mg_g * mg_table[p][2]) / (1.0 * denominator)
                mg_table[p][4] = math.exp(mg_g * mg_table[p][5]) / (1.0 * denominator)
                if(temp == 1):
                    print("Tsekarw human:"+str(p+1))
                    choice0user1_list.append(mg_table[p][1])     # Choice 0 for the graph
                    choice1user1_list.append(mg_table[p][4])     # Choice 1 for the graph
                if (temp == 0):
                    choice0user2_list.append(mg_table[p][1])  # Choice 0 for the graph
                    choice1user2_list.append(mg_table[p][4])  # Choice 1 for the graph
                    iteration_list.append(mg_iteration)
        print("Graph0: "+str(choice0user1_list))
        print("Graph1: "+str(choice1user1_list))

        mg_count = 0
        for p in range(len(selected_routes)):
            if (selected_routes[p] == i + 1):

                if mg_table[p][1] < 0.01:
                    mg_table[p][1] = 0.01
                if mg_table[p][4] < 0.01:
                    mg_table[p][4] = 0.01
                if mg_table[p][1] > 0.99:
                    mg_table[p][1] = 0.99
                if mg_table[p][4] > 0.99:
                    mg_table[p][4] = 0.99

        for p in range(len(players)):
            if (abs(mg_table[players[p]][1] - 1) < 0.1 or abs(mg_table[players[p]][4] - 1) < 0.1):
                mg_count += 1

        if (mg_count == len(players)):
            convergence = 1
            print("MG TABLE:" + str(mg_table))
            print("Evacuating & thres_Convergence:" + str(evacuating_users_temp) + str(":::") + str(
                capacity_route[i] - number_of_humans[i]))
            print("Minority Game is converged...")
            #print ("Choice0_Length:"+str(len(choice0_list)))
            print("IterationList_length:"+str(len(iteration_list)))
            plt.xticks(iteration_list)

            line1, = plt.plot(iteration_list,choice0user1_list,marker='d', label="User1, choice0")
            line2, = plt.plot(iteration_list, choice1user1_list,marker='d', label="User1, choice1")
            line3, = plt.plot(iteration_list, choice0user2_list, marker='o', label="User2, choice0")
            line4, = plt.plot(iteration_list, choice1user2_list, marker='o', label="User2, choice1")
            #plt.set_position([chartBox.x0, chartBox.y0, chartBox.width * 0.6, chartBox.height])
            plt.legend(loc='upper center', bbox_to_anchor=(0.9, 0.8), shadow=True, ncol=1)

            fontP = FontProperties()
            fontP.set_size('small')

            #first_legend = plt.legend(handles=[line1,line2], prop=fontP)
            #ax = plt.gca().add_artist(first_legend)
            #second_legend = plt.legend(handles=[line3, line4], loc=2)
            #ax = plt.gca().add_artist(second_legend)
            #plt.legend(["Choice 0", "Choice 1"], loc='upper left')
            plt.axis([1,len(iteration_list),0,1])
            plt.xlabel('Iterations', fontsize=14)
            plt.ylabel('Probabilities', fontsize=14)
            plt.title("Minority Game choices Convergence")
            plt.show(block=True)
        else:
            mg_iteration += 1
            print("Evacuating & thres_NOTConvergence:" + str(evacuating_users_temp) + str(":::") + str(
                capacity_route[i] - number_of_humans[i]))
    print mg_iteration

minority_games()